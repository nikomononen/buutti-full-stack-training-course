/*
Exercise 8: Right-angled
Create a program that takes in a number n and
prints out a triangle of &’s with the height of n

Level 1: if n = 4, program prints
&
&&
&&&
&&&&

Level 2:
    &
   &&&
  &&&&&
 &&&&&&&

Level 3:
Using the other loop compared to what you
originally used, complete the same task.
E.g. if you solved this using for loops, now solve
it using while loops

*/
function printTriangleLevel1(n){
    for(let i = 1;i <= n; i ++){
        const letters = Array(i).fill("&");
        console.log(letters.join(""));
    }
}
/*
    Create '&' pyramid with given n levels of height
    Example:
    n = 3
    ---
    array = n * 2 - 1 = 5
    [" "," "," "," "," "]

    row = 1
    column: n - row = 3 - 1 = 2
    (n + row) - 1 = 3 + 1 - 1 = 3
    2 < 3  --> 1 symbol staring from index 2
    [" "," ","&"," "," "]
    
    row = 2
    column: n - row = 3 - 2 = 1
    (n + row) - 1 = 3 + 2 - 1 = 4 
    1 < 4 --> 3 symbols staring from index 1
    [" ","&","&","&"," "]
    
    row = 3
    column: n - row = 3 - 3 = 0
    (n + row) - 1 = 3 + 3 - 1 = 5 
    0 < 5 --> 5 symbols staring from index 0
    ["&","&","&","&","&"]
    
    [
        [ ' ', ' ', '&', ' ', ' ' ],
        [ ' ', '&', '&', '&', ' ' ],
        [ '&', '&', '&', '&', '&' ]
    ]

*/
function createPyramidArrayForLoop(n){
    const pyramidArray = [];
    for(let row = 1; row <= n; row++){
        const letters = Array((n * 2 ) - 1).fill(" ");
        for (let column = n - row; column < ((n + row) - 1); column++) {
            letters[column] = "&";
        }
        pyramidArray.push(letters);
    }
    return pyramidArray;
}
function createPyramidArrayWhileLoop(n) {
    const pyramidArray = [];
    let row = 0;
    while(row <= n){
        const letters = Array((n * 2 ) - 1).fill(" ");
        // Start row count of letters from n and continue till (n + row) - 1 count past middle
        let column = n - row;
        while(column < ((n + row) - 1)) {
            letters[column] = "&";
            column++;
        }
        pyramidArray.push(letters);
        row++;
    }
    return pyramidArray;
}

/*
    Print pyramid array to console
*/
function printTriangleLevel2(n){
    const pyramidArray = createPyramidArrayForLoop(n);
    pyramidArray.forEach(line => console.log(line.join("")));
}
/*
    Same as printTriangleLevel2 but with while loop
*/
function printTriangleLevel3(n){
    const pyramidArray = createPyramidArrayWhileLoop(n);
    pyramidArray.forEach(line => console.log(line.join("")));
}

const numberFromCommandline = parseInt(process.argv[2]);
// If not number or below 1, default to 3 so it's meaningful to show anything
if (isNaN(numberFromCommandline) || numberFromCommandline < 1) process.exit(1);
printTriangleLevel1(numberFromCommandline);

console.log("\n");
// Pyramid array
console.log(createPyramidArrayForLoop(numberFromCommandline));

console.log("\n");
// Pyramid with for loop
printTriangleLevel2(numberFromCommandline);
console.log("\n");
// Pyramid with while loop
printTriangleLevel3(numberFromCommandline);