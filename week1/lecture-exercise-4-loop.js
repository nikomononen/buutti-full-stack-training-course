// 1. 0 100 200 300 400 500 600 700 800 900 1000
for (let i = 0; i <= 1000;i++){
    if (i % 100 === 0) console.log(i);
}
console.log("\n");
// 2. 1 2 4 8 16 32 64 128
for (let i = 0; i <= 7;i++){
    console.log(2**i);
}
console.log("\n");
// 3. 3 6 9 12 15
for (let i = 1; i <= 5;i++){
    console.log(3*i);
}
console.log("\n");
// 4. 9 8 7 6 5 4 3 2 1 0
for (let i = 9; i >= 0;i--){
    console.log(i);
}
console.log("\n");
// 5. 1 1 1 2 2 2 3 3 3 4 4 4
for (let i = 1; i <= 4;i++){
    for (let j = 0;j < 3;j++){
        console.log(i);
    }
}
console.log("\n");
// 6. 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4
for (let i = 0; i < 3;i++){
    for (let j = 0;j <= 4;j++){
        console.log(j);
    }
}
