const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

// All
function printArray(array){
    /*
    array.forEach(fruit => {
        console.log(fruit);
    });
    */
    console.log(array.join(" "));
    console.log("\n");
}
console.log("All items in array:");
printArray(arr);

console.log("Items that contain r:");
// Contains r
arr.forEach(fruit => {
    if(fruit.includes("r")) console.log(fruit);
});
console.log("\n");

console.log("Array sorted:");
// Sort alphabetical
arr.sort();
printArray(arr);

console.log("First element removed:");
// Remove first
arr.shift();
printArray(arr);

console.log("Added sipuli at the end:");
// Add sipuli
arr.push("sipuli");
printArray(arr);