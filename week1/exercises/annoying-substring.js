/*
    Create a program that takes in a string and drops off the last word of any given string, and console.logs it out.
    example: node .\annoyingSubstring.js "Hey I'm alive!" -> Hey I'm
*/

/*
    Checks string is valid
*/
function isValidString(string) {
    if (typeof string === "undefined" || string.length < 1) return false;
    return true;
}
/*
    Removes last word from the given message. Returns empty string if there is only one words
*/
function removeLastWordFromMessage(message){
    const wordsAsArray = message.split(" ");
    wordsAsArray.splice(-1,1);
    return wordsAsArray.join(" ");

}
// Read inputs from commandline
const message = process.argv[2];

if (isValidString(message)) {
    const transformedMessage = removeLastWordFromMessage(message);
    console.log(transformedMessage);
}
