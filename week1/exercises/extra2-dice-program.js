import readline from "readline";
// Node line reader settings
const consoleLineReader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Defaults
const defaultDiceNumber = 6;
const minimumDiceNumber = 3;
const maximumDiceNumber = 100;
const exitCodeEnum = {
    SUCCESS: 0,
    FAILURE: 1
};

/*
    function to verify of the input is valid:
        - Starts with D and rest is number
        - Number part is below maximum dice sides allowed
    @param dice_string dice string to check validity
    @return true/false if the given string is valid or not
*/
function diceIsValid(diceString) {
    if(!/^D[0-9]+$/.test(diceString)) return false;
    const diceNumber = parseInt(diceString.substring(1));
    return (!isNaN(diceNumber) && diceNumber <= maximumDiceNumber);
}

/*
    Function to roll a number between 1 and number of sides on the dice
    @param sidesOnTheDice number of sides on the dice
    @return rolled dice number
*/
function rollDice(sidesOnTheDice){
    return Math.floor(Math.random() * sidesOnTheDice) + 1; 
}

/*
   Exit routine
    @param message Message to print to console
    @code exit code for the program
*/
function exitProgram(message, code){
    consoleLineReader.close();
    console.log(message);
    process.exit(code);
}

/*
   Promt loop for user to roll the dice or quit the program
   @param sidesOnTheDice number of sides on the dice
*/
function promptUserToRollOrQuit(sidesOnTheDice){
    // Ask user to roll or quit
    consoleLineReader.question("Press <enter> for a dice roll, type 'q' + <enter> to exit ", function(answer) {
        // Quit if 'q' is entered
        if (answer === "q") return exitProgram("Bye!", exitCodeEnum.SUCCESS);
        // Roll dice
        const roll = rollDice(sidesOnTheDice);
        switch(roll){
            case 1: 
                console.log(`Ouch! the roll is ${roll}`);
                break;
            case sidesOnTheDice: 
                console.log(`Yippii! the roll is ${roll}`);
                break;
            default:
                console.log(`The roll is ${roll}`);
        }
        promptUserToRollOrQuit(sidesOnTheDice);
    });
}

// Read dice number from command line arguments and check if its valid
const diceEntryGivenByUser = process.argv[2];
if(!diceIsValid(diceEntryGivenByUser)) {
    exitProgram(
        `Dice should be given in format D<number> and number between ${minimumDiceNumber} and ${maximumDiceNumber}. Eg. D10`,
        exitCodeEnum.FAILURE
    ); 
}
// Input is D<Number> format
let sidesOnTheDice = parseInt(diceEntryGivenByUser.substring(1));
// Use default if sides of the dice is lower than 3
if (sidesOnTheDice < minimumDiceNumber){
    console.log(`Dice sides count is less than ${minimumDiceNumber}, defaulting to: ${defaultDiceNumber}`);
    sidesOnTheDice = defaultDiceNumber;
}

// Start loop
promptUserToRollOrQuit(sidesOnTheDice);