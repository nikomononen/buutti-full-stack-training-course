/*
    Largest and smallest
    From the command line read in three numbers, number_1, number_2 and number_3. Decide their values freely.
    Find the
    a) largest one
    b) smallest one
    c) if they all are equal, print that out
    console.log() its name and value
*/

/*
    Function get largest number in the given array 

    @param array to process
    @return largest number
*/
function getLargestNumber(array){
    let largestNumber = array[0];
    for (let i = 1; i < array.length; i++) {
        if (array[i] > largestNumber) largestNumber = array[i];
    }
    return largestNumber;
}
/*
    Function get smallest number in the given array 

    @param array to process
    @return smallest number
*/
function getSmallestNumber(array){
    let smallestNumber = array[0];
    for (let i = 1; i < array.length; i++) {
        if (array[i] < smallestNumber) smallestNumber = array[i];
    }
    return smallestNumber;
}

/*
    Function to check if all numbers in given array are equal

    @param array to process
    @return true/false if all numbers in the array are equal
*/
function checkIfNumbersAreEqual(array){
    const first = array[0];
    for (let i = 1; i < array.length; i++) {
        if (array[i] !== first) return false;
    }
    return true;
}

// Read inputs from commandline
const a = parseInt(process.argv[2]);
const b = parseInt(process.argv[3]);
const c = parseInt(process.argv[4]);
const numbersArray = [a,b,c];

// a.
const largestNumber = getLargestNumber(numbersArray);
console.log(`Largest number is ${largestNumber}`);

// b.
const smallestNumber = getSmallestNumber(numbersArray);
console.log(`Smallest number is ${smallestNumber}`);
// c.
if (checkIfNumbersAreEqual(numbersArray)){
    console.log(`All numbers ${a}, ${b} and ${c} are equal`);
}


