// Travel time
const distance = 30; // km
const speed = 7; // km/h

const travel_time = distance / speed;
// hours 
console.log(`Distance: ${distance}km, speed: ${speed}km/h, Travel time: ${travel_time}h`);