/*
    Create a program that takes in one argument from command line, a language code (e.g. "fi", "es", "en"). Console.log "Hello World" for the given language for
    atleast three languages. It should default to console.log "Hello World".
    Remember to test that the program outputs the right answer in all cases.
    Hint: use process.argv for input
*/
const greetings = {
    "fi": "Hei maailma!",
    "en": "Hello world!",
    "jp": "こんにちは世界",
    "notDefined": "Language is not defined."
};
/*
    Returns greeting on given language or not defined message.
*/
function greet(language){
    //if ((Object.prototype.hasOwnProperty.call(greetings, language))) return greetings[language];
    if (language in greetings) return greetings[language];
    else return greetings.notDefined;
}

const languageInput = process.argv[2];
console.log(greet(languageInput));
console.log("\n");
console.log("Tests:");
const testFI = greet("en") == "Hello world!";
const testEN = greet("fi") == "Hei maailma!";
const testJP = greet("jp") == "こんにちは世界";
const testNotDefined = greet("es") == "Language is not defined.";

console.log(`fi: ${testFI}`);
console.log(`en: ${testEN}`);
console.log(`jp: ${testJP}`);
console.log(`not defined: ${testNotDefined}`);