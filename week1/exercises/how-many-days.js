/*
    Create a program that takes in a number from commandline that represents month of the year. Use console.log to show how many days there are in the given
    month number.
*/
const daysInMonth = {
    1: {name: "January", days: 31},
    2: {name: "February", days: 28},
    3: {name: "March", days: 31},
    4: {name: "April", days: 30},
    5: {name: "May", days: 31},
    6: {name: "June", days: 30},
    7: {name: "July", days: 31},
    8: {name: "August", days: 31},
    9: {name: "September", days: 30},
    10: {name: "October", days: 31},
    11: {name: "November", days: 30},
    12: {name: "December", days: 31}
};

/*
    Prints days in given month
*/
function printDaysInMonth(month){
    const monthObject = daysInMonth[month];
    console.log(`In ${monthObject.name} there is ${monthObject.days} days`);
}

// Read inputs from commandline
const monthInput = parseInt(process.argv[2]);
if (monthInput >= 1 && monthInput <= 12) printDaysInMonth(monthInput);
else console.log("Month must be number between 1 and 12");