/*
    Initial letters
    Create a program that takes in 3 names and outputs only initial letters of those name separated with dot.
    example: node .\initialLetters.js Jack Jake Mike -> j.j.m
*/
function printInitialLetters(array){
    const initialLettersArray = [];
    array.forEach(name => {
        initialLettersArray.push(name.charAt(0).toLowerCase());
    });
    console.log(initialLettersArray.join("."));
}
/*
    Checks if there is enough names in the array and that they are defined and atleast 1 character long
*/
function isEnoughNamesGiven(array) {
    if (array.length < 3) return false;
    for (let i = 0; i < array.length; i++){
        const name = array[i];
        if (typeof name === "undefined" || name.length < 1) return false;
    }
    return true;
}

// Read inputs from commandline
const firstName = process.argv[2];
const secondName = process.argv[3];
const thirdName = process.argv[4];

const namesArray = [firstName, secondName, thirdName];
if (isEnoughNamesGiven(namesArray)) printInitialLetters(namesArray);
