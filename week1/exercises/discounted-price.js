
// Discounted price
const price = 29.90;
const discount = 20;

const discounted_price = ((100 - discount) / 100) * price;
console.log(`Price: ${price}$, Discount: ${discount}%, Discounted price: ${discounted_price}$`);
