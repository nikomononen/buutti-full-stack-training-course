/*
    Greater, smaller or equal
    1. Create a program that takes in two numbers a and b from the command line.
    2. Print out "a is greater" if a is bigger than b, and vice versa, and "they are equal" if they are equal
    3. Modify program to take in a third string argument c, and print out "yay, you guessed the password", if a and b are equal AND c is "hello world"
    Remember to test that the program outputs the right answer in all cases.
*/
function printNumbers(a, b){
    switch(true){
        case a > b: {
            console.log(`${a} is greater than ${b}`);
            break;
        } 
        case a < b: {
            console.log(`${b} is greater than a: ${a}`);
            break;
        }
        case a === b: {
            console.log(`${a} and ${b} are equal`);
            break;
        }
        default: {
            console.log("Error happened.");
        }       
    }
}
const a = parseInt(process.argv[2]);
const b = parseInt(process.argv[3]);
printNumbers(a, b);