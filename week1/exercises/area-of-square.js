// Area of square
const square_size = parseFloat(process.argv[2]);
const square_area = square_size * square_size;
console.log(`Square area for side of ${square_size} is ${square_area}`);