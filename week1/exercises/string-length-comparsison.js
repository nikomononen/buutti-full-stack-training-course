/*
    String length comparison
    Create a program that takes in 3 names, and compares the length of those names. Print out the names ordered so that the longest name is first.
    example: node .\lengthcomparison.js Maria Joe Philippa -> Philippa Maria Joe
*/

function printSortedByLongestNameArray(array){
    array.sort(function(a, b){
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        return b.length - a.length;
    });
    console.log(array.join(" "));
}
/*
    Checks if there is enough names in the array and that they are defined and atleast 1 character long
*/
function isEnoughNamesGiven(array) {
    if (array.length < 3) return false;
    for (let i = 0; i < array.length; i++){
        const name = array[i];
        if (typeof name === "undefined" || name.length < 1) return false;
    }
    return true;
}

// Read inputs from commandline
const firstName = process.argv[2];
const secondName = process.argv[3];
const thirdName = process.argv[4];

const namesArray = [firstName, secondName, thirdName];
if (isEnoughNamesGiven(namesArray)) printSortedByLongestNameArray(namesArray);
