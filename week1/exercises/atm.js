/*
    Create a ATM program to check your balance. Create variables balance , isActive , checkBalance . Write conditional statement that implements the flowchart
    below.
    Change the values of balance , checkBalance , and isActive to test your code!
*/
import readline from "readline";
// Node line reader settings
const consoleLineReader = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Defaults
const account = {
    balance: -1,
    checkBalance: 100,
    isActive: true
};
const exitCodeEnum = {
    SUCCESS: 0,
    FAILURE: 1
};

/*
   Exit routine
    @param message Message to print to console
    @code exit code for the program
*/
function exitProgram(message, code){
    consoleLineReader.close();
    console.log(message);
    return process.exit(code);
}

function checkAccountIsActive(){
    return account.isActive;
}
function getBalance(){
    return account.balance;
}

/*
    Function to check account and print balance if account is active
    otherwise print account is not active. 
*/
function checkAccount(){
    const balance = getBalance();
    if (checkAccountIsActive()){
        let balanceMessage;
        if (balance > 0) balanceMessage = `Your account balance is ${balance}`;
        else if (balance < 0) balanceMessage = "Your account balance is negative";
        else if (balance === 0) balanceMessage = "Your account is empty";
        else exitProgram("Error happened while checking account balance.", exitCodeEnum.FAILURE);
        // Print out account balance and quit program
        exitProgram(balanceMessage, exitCodeEnum.SUCCESS);
    } else {
        exitProgram("Your account is not active", exitCodeEnum.SUCCESS);
    }
}
function quitATM(){
    exitProgram("Have a nice day!", exitCodeEnum.SUCCESS);
}

/*
   Promt user with given message and check for yes/no answer
*/
function promtUser(message, yesCallback, noCallback){
    consoleLineReader.question(message, function(answer) {
        switch(answer){
            case "yes":
                yesCallback();
                break;
            case "no":
                noCallback();
                break;
            default:
            // If no valid input is give, ask again
                promtUser(message, yesCallback, noCallback);
        }
    });
}

// Start ATM
promtUser("Check your balance? ", checkAccount, quitATM);
