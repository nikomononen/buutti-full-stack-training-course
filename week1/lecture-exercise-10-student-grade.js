/*
   ● Find the highest, lowest scoring person.
   ● Then find the average score of the students.
   ● Then print out only the students who scored higher than the average.
   ● Assign grades 1-5 for each student based on their scores like this:
    "1": "1-39",
    "2": "40-59",
    "3": "60-79",
    "4": "80-94",
    "5": "95-100"
   
*/
const students = [
    {name: "markku", score: 99}, {name: "karoliina", score: 58},
    {name: "susanna", score: 69}, {name: "benjamin", score: 77},
    {name: "isak", score: 49}, {name: "liisa", score: 89},
];
const grades = {
    "1": "1-39",
    "2": "40-59",
    "3": "60-79",
    "4": "80-94",
    "5": "95-100"
};
/*
    Finds student with highest score of given array
    @param studentsArray array of students to process
    @return student with highest score in the given array
*/
function findHighestScoretudent(studentsArray){
    let highestScoreStudent = studentsArray[0];
    for (let i = 1; i < studentsArray.length; i++) {
        const student = studentsArray[i];
        if (student.score > highestScoreStudent.score) highestScoreStudent = student;
    }
    return highestScoreStudent;
}
/*
    Finds student with lowest score of given array
    @param studentsArray array of students to process
    @return student with lowest score in the given array
*/
function findLowestScoretudent(studentsArray){
    let lowestScoreStudent = studentsArray[0];
    for (let i = 1; i < studentsArray.length; i++) {
        const student = studentsArray[i];
        if (student.score < lowestScoreStudent.score) lowestScoreStudent = student;
    }
    return lowestScoreStudent;
}
/*
    Finds average score for students in given array
    @param studentsArray array of students to process
    @return average score of students
*/
function findAverageScore(studentsArray){
    let averageScore = 0;
    studentsArray.forEach(student => {
        averageScore += student.score;
    });
    return averageScore / studentsArray.length;
}

/*
    Finds students from array that has score higher than average

    @param studentsArray array of students to process
    @return array of students that has higher score than average of given array
*/
function findStudentsAboveAverageScore(studentsArray) {
    const averageScore = findAverageScore(studentsArray);
    const studentsAboveAverageScore = [];
    studentsArray.forEach(student => {
        if (student.score > averageScore) {
            studentsAboveAverageScore.push(student);
        }
    });
    return studentsAboveAverageScore;
}
/*
    Function to grade students

    @param studendsArray array of students to grade
    @param gradesObject object of grades in format of 'grade: score range'
    @return array of graded students
*/
function gradeStudents(studentsArray, gradesObject){
    const gradedStudents = [];
    studentsArray.forEach(student => {
        for (const [grade, gradeRange] of Object.entries(gradesObject)) {
            const minMaxGradesArray = gradeRange.split("-");
            const min = parseInt(minMaxGradesArray[0]);
            const max = parseInt(minMaxGradesArray[1]);
            if (student.score >= min && student.score <= max) {
                student.grade = grade;
                break;
            }
        }
        gradedStudents.push(student);
    });
    return gradedStudents;
}

const highestScoreStudent = findHighestScoretudent(students);
console.log(`Highest score student is ${highestScoreStudent.name} with score of ${highestScoreStudent.score}`);

const lowestScoreStudent = findLowestScoretudent(students);
console.log(`Lowest score student is ${lowestScoreStudent.name} with score of ${lowestScoreStudent.score}`);

const averageScore = findAverageScore(students);
console.log(`Average score is ${averageScore}`);

console.log("Students above average score is:");
const studentsAboveAverageScore = findStudentsAboveAverageScore(students);
console.log(studentsAboveAverageScore);

console.log("Graded students:");
const gradedStudents = gradeStudents(students, grades);
console.log(gradedStudents);