console.log("Hello world!");
const a = parseFloat(process.argv[2]);
const b = parseFloat(process.argv[3]);

console.log("Summa: " + (a + b));
console.log("Erotus: " + (a - b) );
console.log("Osamäärä: " + (a / b));
console.log("Tulo: " + (a * b));
console.log("Jakojäännös: " + (a % b));
console.log("Potenssi: " + (a ** b));

// Functions
function average(list){
    const list_sum = list.reduce((sum, num) => sum + num);
    return list_sum / list.length;
}
function string_average(list){
    const list_sum = list.reduce((sum, str) => sum + str.length, 0);
    return list_sum / list.length;
}

const numbers_list = [a,b];
console.log("Keskiarvo: " + average(numbers_list));

const str0 = " Matti";
str0.trim();
console.log(str0);
const str0_trimmed = str0.trim();
console.log(str0_trimmed);

const str1 = "Matti";
const str2 = "Meikäläinen";
console.log(`${str1} pituus on: ` + str1.length);
console.log(str1 + str2);
console.log(`My name is ${str1} ${str2}. Hello world!`);

const str_avg = (str1.length + str2.length) / 2;
console.log(`Sanojen ${str1} ja ${str2} keskiarvopituus on: ${str_avg}`);

const str_list = [str1, str2];
const str_avg1 = string_average(str_list);
console.log(str_avg1);

const short_str = "test";
// Jos sana on lyhyempi kuin keskiarvo tulostetaan
if (short_str.length < str_avg) console.log(short_str);

// Logic
// a.
const playerCount = 5;
if (playerCount == 4) console.log("Lets play hearts");
else console.log("Pelaajia ei ole oikeaa määrää");
// b.
const isStressed = false;
const hasIcecream = true;
if(!isStressed || hasIcecream) console.log("Mark is happy");
// c.
const shining = true;
const raining = false;
const temperature = 21;
if (shining && !raining && temperature >= 20) console.log("Beach day!");
// d.
const suzy = true;
const dan = false;
const weekDay = "Tuesday";
// Equivalent
// if ( suzy !== dan && weekDay === "Tuesday") console.log("Arin happy.");
if (((suzy && !dan) || (!suzy && dan)) && weekDay === "Tuesday") console.log("Arin happy.");
else console.log("Arin sad.");

const str6 = " This is a test string which is very long.";
// Exercise 6
function sanitize_string(string){
    let str = string.trim();
    const firstLetter = str.substring(0,1);
    str = str.substring(1,20);
    str = firstLetter.toLowerCase() + str;
    return str;
}
// this is a test strin
console.log(sanitize_string(str6));

const whitespace_chars = [" ", "\t", "\n"];
// Bonus
function check_string_sanity(string){
    let contains_illegal = false;
    // Merkkijonon ei saa alkaa eikä loppua white space
    if (string.startsWith(whitespace_chars) || string.endsWith(whitespace_chars)) contains_illegal = true;
    // Merkkijonon on altava 20 merkkiä tai alle
    if (string.length > 20) contains_illegal = true;
    // Eka merkki ei saa alkaa isolla
    const firstLetter = string.substring(0,1);
    if (firstLetter.toUpperCase() == firstLetter) contains_illegal = true;
    if (contains_illegal) return "Merkkijonossa on kiellettyjä asioita";
    else return "Merkkijono on ok";
}
console.log(check_string_sanity(str6));
console.log(check_string_sanity("tämä on ok"));