// Sum of integers multiples of three or five to n with given number n
function sumOfMultiplesOf3Or5WithForLoop(n){
    let sum = 0;
    for (let i = 1; i <= n; i++){
        if (i % 3 === 0 || i % 5 === 0) sum += i;      
    }
    return sum;
}
function sumOfMultiplesOf3Or5WithWhileLoop(n){
    let sum = 0;
    let i = 0;
    while(i <= n){
        if (i % 3 === 0 || i % 5 === 0) sum += i;
        i++;      
    }
    return sum;
}
const numberFromCommandline = parseInt(process.argv[2]);
console.log(sumOfMultiplesOf3Or5WithForLoop(numberFromCommandline));
console.log(sumOfMultiplesOf3Or5WithWhileLoop(numberFromCommandline));