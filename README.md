This is exercise repository for [Buutti Full Stack Training](https://buuttiedu.com/tyonhakijalle/buutti-full-stack-training-rekrykoulutus/) course.

Course content
- Full Stack development
- JavaScript (ES6)
- Node
- Express
- MongoDB
- React
- UI/UX design basics, HTML/CSS prototypes
- Scrum
- Git
- GitLab CI/CD-workflow
- Personal studies, Angular

Notable projects
- Command line [Hangman game](https://gitlab.com/NikoMononen/buutti-full-stack-training-course/-/blob/main/week2/projects/command-line-hangman-tree.js)
- Command line [Not a Wordle game](https://gitlab.com/NikoMononen/buutti-full-stack-training-course/-/blob/main/week2/projects/command-line-not-a-wordle.js)
- Group blue project - [Command line contacts application](https://gitlab.com/NikoMononen/buutti-full-stack-training-project-contacts)
- [Buutti banking API](https://gitlab.com/NikoMononen/buutti-full-stack-training-course/-/tree/main/week5/exercises/exercise-buutti-banking) with Node + Express + MongoDB
- [React frontend](https://gitlab.com/NikoMononen/buutti-full-stack-training-course/-/tree/main/week7/exercises/fake-store-api) for [Fake Store API](https://fakestoreapi.com/)
- Group blue project - [Library app with React](https://gitlab.com/jarno-severi/group-blue-library-app)