import express from "express";
import bodyParser from "body-parser";

/*

GET http://localhost:5000/dessert/cupcake?yes=please
Content-Type: application/json

{
    "Cupcake" : "🧁"
}

*/
const app = express();
const port = 5000;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Middleware logger
app.use("*", (request, response, next) => {
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    console.log(`Request: ${request.method} ${fullUrl}`);
    if (request.body !== undefined ) {
        console.log(`Request body: ${JSON.stringify(request.body)}`);
    }

    if (Object.entries(request.query).length > 0){
        console.log(`Request query params: ${JSON.stringify(request.query)}`);
    }

    if (Object.entries(request.params).length > 0){
        console.log(`Request params: ${JSON.stringify(request.params)}`);
    }
    next();
});

app.all("/dessert/:type", (request, response) => {
    response.send("Hello world!");
});

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});