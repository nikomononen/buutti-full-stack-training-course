import express from "express";
import fs from "fs";
const app = express();
const port = 5000;

let counter = 0;
const counterObject = {};

// Middleware to write log file
app.use("*", (request, response, next) => {
    const now = new Date().toISOString();
    const logString = `[${now}] ${request.method} ${request.baseUrl}`;
    fs.appendFile("lecture-exercise-3-counter-log.txt", logString + "\n", function (err) {
        if (err) return console.log(err);
        console.log(logString);
        next();
    });
});

app.get("/", (request, response) => {
    response.send("Hello world!");
});

app.get("/counter", (req, res) => {
    if ("number" in req.query)  counter = parseInt(req.query.number);
    else counter++;

    res.send(`<h1>Counter has been accessed ${counter} times.</h1>`);
});

app.get("/counter/:name", (req, res) => {
    if ("name" in req.params) {
        const id = req.params.name;
        if (id in counterObject) counterObject[id] += 1;
        else counterObject[id] = 1;
    }

    const response = ["<h1>Counter</h1>"];
    const counterObjectEntries  = Object.entries(counterObject);

    if (counterObjectEntries.length > 0){
        for (let i = 0; i < counterObjectEntries.length; i++){
            const [key, value ] = counterObjectEntries[i];
            response.push(`<p>${key} was here ${value} times</p>`);
        }
    }
    
    res.send(response.join("\n"));
});

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});