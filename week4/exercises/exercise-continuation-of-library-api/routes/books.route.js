import express from "express";
import * as BooksController from "../controllers/books.controller.js";
import { authorizationMiddleware } from "../controllers/authentication.controller.js";

const router = express.Router();

// Use JWT authentication middleware
router.use(authorizationMiddleware);

// User books
router.get("/books", BooksController.getAllBooks);
router.get("/books/:id", BooksController.getBook);
router.post("/books", BooksController.postBook);
router.put("/books/:id", BooksController.putBook);
router.delete("books/:id", BooksController.deleteBook);

// Friend books
router.get("/friendbooks", BooksController.getAllFriendBooks);
router.get("/friendbooks/:id", BooksController.getFriendBooks);

export default router;