import express from "express";
import * as AuthenticationController from "../controllers/authentication.controller.js";

const router = express.Router();

router.post("/login", AuthenticationController.login);
router.post("/register", AuthenticationController.register);

export default router;