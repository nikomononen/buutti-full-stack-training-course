import express from "express";
import * as UsersController from "../controllers/users.controller.js";
import { authorizationMiddleware } from "../controllers/authentication.controller.js";

const router = express.Router();

// Use JWT authentication middleware
router.use(authorizationMiddleware);

router.get("/users", UsersController.getAllUsers);
router.get("/friends", UsersController.getFriends);
router.post("/friends", UsersController.addFriend);

export default router;