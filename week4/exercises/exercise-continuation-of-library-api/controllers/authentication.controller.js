import uuid4 from "uuid4";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { User } from "../models/user.model.js";
import { users } from "../controllers/users.controller.js";

const bcryptHashRounds = 10;

// Authorization middleware
export const authorizationMiddleware = (request, response, next) => {
    const authToken = request.get("authorization");
    if (authToken && authToken.startsWith("Bearer ")) {
        try {
            // Substring "Bearer " and verify token
            const token = authToken.substring(7);
            const decodedToken = jwt.verify(token, process.env.APP_SECRET);

            if (!decodedToken.username) {
                return response.status(403).json({
                    error: "Access token is invalid"
                });
            } else {
                request.authenticatedUser = users.find((user) => user.id === decodedToken.id);
                next();
            }
        }
        catch (error) {
            switch (error.constructor) {
                case jwt.TokenExpiredError: {
                    return response.status(403).json({
                        error: error.message
                    });
                }
                case jwt.JsonWebTokenError: {
                    return response.status(401).json({
                        error: error.message
                    });
                }
                default: {
                    return response.status(500).json({
                        error: error.message
                    });
                }
            }
        }
    } else {
        return response.status(401).json({
            error: "Auth token missing"
        });
    }
};

/**
 * Login. Creates and returns JWT token on successful login
 */
export const login = async (request, response) => {
    const username = request.body.username;
    const password = request.body.password;
    const user = users.find((user) => user.username === username);

    if (user) {
        try {
            const passwordIsCorrect = await bcrypt.compare(password, user.password);
            if (passwordIsCorrect) {
                // Sign JWT token
                try {
                    const token = jwt.sign(
                        { 
                            username: user.username, 
                            id: user.id 
                        },
                        process.env.APP_SECRET,
                        { expiresIn: "1h" }
                    );
                    response.status(200).json({
                        "status": "Access granted!",
                        "token": token
                    });
                } catch (error) {
                    // jwt.sign thrown error
                    response.status(500).json({
                        "error": error.message
                    });
                }
            }
            else {
                response.status(403).json({
                    "status": "Access denied!"
                });
            }
        }
        catch (error) {
            // bcrypt.compare thrown error
            response.status(500).json({
                "error": error.message
            });
        }
    } else {
        response.status(401).json({
            "status": "Access denied!"
        });
    }
};

/**
 * Register new user
 */
export const register = async (request, response) => {
    const index = users.findIndex(user => {
        return user.username === request.body.username;
    });

    // Username not in use
    if (index === -1) {
        const user = new User({ ...request.body, 
            id : uuid4(),
            password: await bcrypt.hash(request.body.password, bcryptHashRounds),
            friends: []
        });
        users.push(user);
        response.status(200).json(user);
    }
    else {
        response.status(401).json({
            "status": "Username already exists"
        });
    }
};


