export const users = [];

/**
 * Returns all usernames in the database
 * @param {Request} request
 * @param {Response} response
 */
export const getAllUsers = (request, response) => {
    response.status(200).json(users.map((user) => user.username));
};

/**
 * Returns usernames list of authenticated user friends
 * @param {Request} request
 * @param {Response} response
 */
export const getFriends = (request, response) => {
    const authenticatedUserFriends = request.authenticatedUser.friends.map((userId) => {
        const user = users.find((user) => user.id === userId);
        if (user) return user.username;
    });
    response.status(200).json(authenticatedUserFriends);
 
};

/**
 * Adds user to authenticated users friend list
 * @param {Request} request
 * @param {Response} response
 */
export const addFriend = (request, response) => {
    const userToAdd = users.find((user) => user.username === request.body.username);
    if (userToAdd) {
        if (!request.authenticatedUser.friends.includes(userToAdd.id)){
            request.authenticatedUser.friends.push(userToAdd.id);
            response.status(200).json({status: "User added to friends"});
        } else {
            response.status(409).json({ error: "User already in friends"});
        }
    } else {
        response.status(404).json({ error: "User not found"});
    }
};

