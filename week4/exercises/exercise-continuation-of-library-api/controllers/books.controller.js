import uuid4 from "uuid4";
import { Book } from "../models/book.model.js";
import { users } from "../controllers/users.controller.js";

//TODO Do not use cached DB
export const books = [];

/**
 * Get all books in the database
 * @param {Request} request
 * @param {Response} response
 */
export const getAllBooks = (request, response) => {
    response.status(200).json(books.filter((book) => book.owner === request.authenticatedUser.id));
};

/**
 * Get specified book from the database
 * @param {Request} request
 * @param {Response} response
 */
export const getBook = (request, response) => {
    const index = books.findIndex(book => {
        return book.id === request.params.id && book.owner ===  request.authenticatedUser.id;
    });

    if (index >= 0) {
        response.status(200).json(books[index]);
    } else {
        response.status(403).json({ error: "Book not found" });
    }
};

/**
 * Add book to database
 * @param {Request} request
 * @param {Response} response
 */
export const postBook = (request, response) => {
    const book = new Book({ ...request.body, 
        id : uuid4(),
        owner: request.authenticatedUser.id });
    books.push(book);
    response.status(200).json(book);
};

/**
 * Update book in the database
 * @param {Request} request
 * @param {Response} response
 */
export const putBook = (request, response) => {
    const index = books.findIndex(book => {
        return book.id === request.params.id && book.owner === request.authenticatedUser.id;
    });

    if (index >= 0) {
        const book = new Book({...request.body, 
            id: books[index].id,
            owner: request.authenticatedUser.id
        });
        books[index] = book;
        response.status(200).json(book);
    } else {
        response.status(403).json({ error: "Book not found" });
    }
};

/**
 * Deletes book from the database
 * @param {Request} request
 * @param {Response} response
 */
export const deleteBook = (request, response) => {
    const index = books.findIndex(book => {
        return book.id === request.params.id && book.owner === request.authenticatedUser.id;
    });

    if (index >= 0) {
        books.splice(index, 1);
        response.status(200).json();
    } else {
        response.status(403).json({ error: "Book not found" });
    }
};

/**
 * Get all friends books in the database
 * @param {Request} request
 * @param {Response} response
 */
export const getAllFriendBooks = (request, response) => {
    response.status(200).json(books.filter((book) => request.authenticatedUser.friends.includes(book.owner)));
};

/**
 * Get all specified friends books in the database
 * @param {Request} request
 * @param {Response} response
 */
export const getFriendBooks = (request, response) => {
    const username = request.params.id;
    const user = users.find((user) => user.username === username);
    if (user !== undefined && request.authenticatedUser.friends.includes(user.id)) {
        response.status(200).json(books.filter((book) => book.owner === user.id));
    } else {
        response.status(403).json({ error: "Friend not in user friend list" });
    }
};