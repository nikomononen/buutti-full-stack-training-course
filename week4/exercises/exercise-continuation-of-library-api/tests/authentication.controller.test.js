import { getMockReq, getMockRes } from "@jest-mock/express";
import { jest } from "@jest/globals";
import { User } from "../models/user.model.js";
import { users } from "../controllers/users.controller.js";
import * as AuthenticationController  from "../controllers/authentication.controller.js";

// eslint-disable-next-line no-unused-vars
const { res, next, mockClear } = getMockRes();

// Data
const mockDatabase = [
    {
        "id":"d115b98a-150f-46f6-b6e5-5fbb34f0e5a2",
        "username":"user",
        "password":"$2b$10$pIPZqL9UPuaYEKtJYMRYn.nWWXRb0u4h/jdFlHfBpmuduAkS8HxrK",
        "friends": ["6144c309-4b59-4542-81d0-22867862739a"]
    },
    {
        "id":"6144c309-4b59-4542-81d0-22867862739a",
        "username":"user2",
        "password":"$2b$10$kQuy47qkjfDaDAfdri6vVegSgBsLtpy.TqC9pd3OkeuoZX3Gs64hm",
        "friends":[]
    }
];

describe("Authentication controller tests' ", () => {
    beforeEach(async ()  => {
        jest.resetModules(); // Clear the cache
        mockClear();
        console.info = jest.fn();
        mockDatabase.forEach(user => users.push(user));

    });
      
    test("login should 200 and return access token", async () => {
        const req = getMockReq({
            body:  { 
                username: "user", 
                password: "password" 
            }
        });

        await AuthenticationController.login(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.objectContaining({
            token: expect.any(String)
        }));
    });

    test("login should 401 when user not found and return error message", async () => {
        const req = getMockReq({
            body:  { 
                username: "usera", 
                password: "password" 
            }
        });
        
        await AuthenticationController.login(req, res);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            "status": "Access denied!" 
        });

    });

    test("should 403 when user password does not match and return error message", async () => {
        const req = getMockReq({
            body:  { 
                username: "user", 
                password: "passwor" 
            }
        });
        
        await AuthenticationController.login(req, res);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({ 
            "status": "Access denied!" 
        });
    });

    test("should 200 and return registered user", async () => {
        const req = getMockReq({
            body:  { 
                username: "newUser", 
                password: "password", 
            }
        });
        const user = new User(req.body);
        await AuthenticationController.register(req, res);

        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith(expect.objectContaining({
            username: user.username,
            password: expect.any(String),
            id: expect.any(String),
            friends: []
        }));
    });

    test("should 401 and return error user already exists", async () => {
        const req = getMockReq({
            body:  { 
                username: "user", 
                password: "password" 
            }
        });
        await AuthenticationController.register(req, res);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledTimes(1);
        expect(res.json).toHaveBeenCalledWith({
            "status": "Username already exists"
        });
    });
});