const allowedFields = ["id", "username", "password", "friends"];

/**
 * Creates User object
 * //TODO add type validation
 * @param {Object} data to be added
 * @returns {User}
 */
function User(object) {
    allowedFields.forEach(field => {
        this[field] = object[field];
    });
}

export { User };
