import fs from "fs";
import uuid4 from "uuid4";
import { databaseFile } from "../app.js";
import { Book } from "../models/book.model.js";

let books;

/**
 * Save database to JSON file
 */
export const saveDatabase = () =>{
    fs.writeFile(databaseFile, JSON.stringify(books),(err) => {
        if (err) console.log(err);
        console.log("Books saved to database");
        console.log(books);
    });
};

/**
 * Read database from JSON file to cached variable
 */
export const readDatabase = () => {
    try {
        books = JSON.parse(fs.readFileSync(databaseFile, {encoding:"utf8", flag:"r"}));
        console.log("Read from database");
    } catch(error) {
        books = [];
        console.log("Creating new database");
    }
    console.log(books);
};

/**
 * Get all books in the database
 * @param {Request} req
 * @param {Response} res
 */
export const getAllBooks = (req, res) => {
    res.send(books);
};

/**
 * Get specified book from the database
 * @param {Request} req
 * @param {Response} res
 */
export const getBook = (req, res) => {
    const index = books.findIndex(student => {
        return student.id === req.params.id;
    });

    if (index >= 0) {
        res.send(books[index]);
    } else {
        res.status(500).send({ error: "Book not found" });
    }
};

/**
 * Add book to database
 * @param {Request} req
 * @param {Response} res
 */
export const postBook = (req, res) => {
    const index = books.findIndex(student => {
        return student.id === req.body.id;
    });

    if (index === -1) {
        const book = new Book({ ...req.body, id : uuid4() });
        books.push(book);
        res.send(book);
    }
    else {
        res.status(500).send({
            error: "Book with given id already exists"
        });
    }
};

/**
 * Update book in the database
 * @param {Request} req
 * @param {Response} res
 */
export const putBook = (req, res) => {
    const index = books.findIndex(student => {
        return student.id === req.params.id;
    });

    if (index >= 0) {
        const book = new Book({...req.body, id: books[index].id });
        books[index] = book;
        res.send(book);
    } else {
        res.status(500).send({ error: "Book not found" });
    }
};

/**
 * Deletes book from the database
 * @param {Request} req
 * @param {Response} res
 */
export const deleteBook = (req, res) => {
    const index = books.findIndex(book => {
        return book.id === req.params.id;
    });

    if (index >= 0) {
        books.splice(index, 1);
        res.send();
    } else {
        res.status(500).send({ error: "Book not found" });
    }
};
