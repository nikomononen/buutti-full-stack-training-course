const allowedFields = ["id", "name", "author", "read"];

/**
 * Creates Book object
 * //TODO add type validation
 * @param {Object} data to be added
 * @returns {Book}
 */
function Book(object) {
    allowedFields.forEach(field => {
        // TODO better
        if (field === "read") this[field] = (object[field] === "true");
        else this[field] = object[field];
    });
}

export { Book };
