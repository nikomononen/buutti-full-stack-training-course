import express from "express";

const app = express();
const port = 5000;

app.get("/", (request, response) => {
    response.send("Hello world!");
});

app.get("/cupcake", (request, response) => {
    response.send("Here, have a 🧁");
});

app.listen(port, () => {
    console.log(`Listening to port ${port}`);
});