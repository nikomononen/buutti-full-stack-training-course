import { multiply } from "./lecture-day-2-exercise-jest-testing";

describe("Multiply function", () => {
    const a = 2;
    const b = 10;

    test("it multiply given a and b to be a * b", () => {
        expect(multiply(a, b)).toBe(a * b);
    });

    test("it is not undefined", () => {
        expect(multiply(a, b)).not.toBeUndefined();
    });

    test("it multiply given a and b not to be a", () => {
        expect(multiply(a, b)).not.toBe(a);
        expect(multiply(a, b)).not.toBe(b);
    });
    
    test("it does not multiply strings", () => {
        expect(multiply("a", "b")).toBe(NaN);
    });
});