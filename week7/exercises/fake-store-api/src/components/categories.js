// No category category for products
export const allProductsCategory = "all products";

// List given list of products
export const SelectedCategoryProductList = ({products, activeProductCategory, selectProduct}) => {
    // Check if under some category or all
    let categoryProducts = activeProductCategory === allProductsCategory?
      products : products.filter(product => product.category === activeProductCategory);
  
    // Create elements
    const productsList = categoryProducts.map((product) => {
      return (
        <div key={product.id} className="product-list-item">
          <p onClick={() => selectProduct(product)}>
            <span className="line-product-id">{product.id}</span>
            <span className="line-product-title">{product.title}</span> 
            <span className="line-product-price">{product.price}Ð</span>
          </p>
        </div>
      )
    });
    
    return (
      <div className="row product-list">
        {productsList}
      </div>
    );
  }
  
// Create select for productCategories
export const ProductCategories = ({productCategories, getProductsInCategory}) => {
    const productCategoriesList = productCategories.map(category => {
    return (
        <option key={category} value={category}>{category}</option>
        );
    });

    return (
    <select className="form-select" onChange={getProductsInCategory}>
        {productCategoriesList}
    </select>
    );
}
