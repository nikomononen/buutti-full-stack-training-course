import React, { useState, useEffect, useReducer } from 'react';
import { allProductsCategory } from "./categories.js";
export const NEWPRODUCTID = -1;

// Single product
export const Product = ({product, updateProduct, deleteProduct, addProduct, productCategories}) => {
    const [editMode, setEditMode] = useState(false);
    // Get product from product reducer
    const productState = product.data;
    const [formState, dispatch] = useReducer(editProductReducer, productState);

    // If product changes reset
    useEffect(() => {
        setEditMode(false);
        dispatch({type:"set", data: productState});
    }, [product]);

    // Handle submit
    const submitHandler = (event) => {
        event.preventDefault();

        // If id is -1 Create new TODO better way all this -1 thing
        if (formState.id === NEWPRODUCTID) {
            // API call
            fetch(`https://fakestoreapi.com/products/`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formState)
            })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then(json => {
                console.log(json);
                // Locally add
               addProduct(json);
            })
            .catch(error => {
                console.log(error);
            })
        }
        else {
            // Update
            fetch(`https://fakestoreapi.com/products/${productState.id}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(formState)
            })
            .then(response => {
                if (response.status === 200) {
                    return response.json()
                }
            })
            .then(json => {
                // Call to update actually
                console.log(json);
                updateProduct(json);
            })
            .catch(error => {
                console.log(error);
            })
        }
    }

    // Delete product from api
    const tryDeleteProduct = () => {
        // API call
        fetch(`https://fakestoreapi.com/products/${product.data.id}`, {
            method:"DELETE",
        })
        .then(response => {
            if (response.status === 200) {
                return response.json()
            }
        })
        .then(json => {
            console.log(json);
            // Call to update actually
            deleteProduct(product.data.id);
        })
        .catch(error => {
            console.log(error);
        })
    }

    // No render when active product is null
    if (productState === null) return(null);
   
    return (
        <>
        { editMode || productState.id === NEWPRODUCTID ? 
            <EditProduct 
                formState={formState}
                dispatch={dispatch}
                categories={productCategories} 
                setEditMode={setEditMode}
                tryDeleteProduct={tryDeleteProduct}
                submitHandler={submitHandler} />
            : 
            <PresentProduct 
                setEditMode={setEditMode}
                product={productState} 
            /> }
        </>
    )
}

// Form reducer
const editProductReducer = (state, event) => {
    switch (event.type) {
        case "updateInput":
        case "updateSelect":
        case "updateTextArea":
            return {
                ...state,
                [event.data.target]: event.data.value
              }
        case "set":
            return event.data
        default:
        return state
    }
}

// EditMode of the product
const EditProduct = ({formState, dispatch, categories, setEditMode, tryDeleteProduct, submitHandler}) => {
    // Select options
    const categorySelectOptions = categories.map(category => {
        if (category === allProductsCategory) return null;
        return (
            <option key={category} value={category}>{category}</option>
        );
    });

    return (
    <>
    <h2>{formState.id === NEWPRODUCTID ? "Add Product" : "Edit Product"}</h2>
    <form onSubmit={submitHandler} className="mb-5">
        <div className="row">
        <div className="form-group mb-2">
            <label htmlFor="title">Title</label>
            <input type="text" className="form-control" id="title" placeholder="Title" value={formState.title}
                 onChange={e => dispatch({type: "updateInput", data: {target:"title", value: e.target.value}})} />
        </div>
        <div className="form-group mb-2">
            <label htmlFor="category">Category</label>
            <select className="form-control" id="category" placeholder="Category" value={formState.category} 
                onChange={e => dispatch({type: "updateSelect", data: {target:"category", value: e.target.options[e.target.selectedIndex].value}})} >
            {categorySelectOptions}
            </select>
        </div>
        <div className="form-group mb-2">
            <label htmlFor="description">Description</label>
            <textarea className="form-control" id="description" placeholder="Product description" rows="4" value={formState.description} 
                onChange={e => dispatch({type: "updateTextArea", data: {target:"description", value: e.target.value}})} />
        </div>
        <div className="form-group mb-2">
            <label htmlFor="price">Price</label>
            <input type="number" className="form-control" id="price" min="0" step="0.01" placeholder="Price" value={formState.price} 
                onChange={e => dispatch({type: "updateInput", data: {target:"price", value: e.target.value}})} />
        </div>
        <div className="form-group mb-2">
            <label htmlFor="image">Image</label>
            <input type="text" className="form-control" id="image" placeholder="Image url" value={formState.image} 
                onChange={e => dispatch({type: "updateInput", data: {target:"image", value: e.target.value}})} />
        </div>
        <input type="hidden" value={formState.id} id="productId" />
        </div>
        <div className="btn-group mt-2">
            { formState.id === NEWPRODUCTID ? 
            <>
                <p><input type="submit" className="btn btn-primary" value="Create" /></p>
            </>
             : 
             <>
                <div className="button-group">
                    <input type="submit" className="btn btn-primary mr-2" value="Save" /> 
                    <input type="button" className="btn btn-danger mr-2" value="Delete" onClick={() => tryDeleteProduct()} /> 
                    <input type="button" className="btn btn-secondary" value="Cancel" onClick={() => setEditMode(false)}/> 
                </div>
            </>
            }
        </div>
        </form>
    </>
)
}

// Presentation of the product
const PresentProduct = ({product, setEditMode}) => {
    if (product.id === NEWPRODUCTID) return null;
    return (
        <>
        <div className="row product-image">
            <img src={product.image} title={product.title}/>
        </div>
        <div className="row">
            <div className="product-title"><p>{product.title}</p></div>
            <div className="product-category"><p>{product.category}</p></div>
            <div className="product-description"><p>{product.description}</p></div>
            <div className="product-price"><p>{product.price}Ð</p></div>
            </div>
            <div className="row">
                <p><input type="button" className="btn btn-primary" onClick={() => setEditMode(true)} value="Edit" /></p>
            </div>
        </>
    )
}