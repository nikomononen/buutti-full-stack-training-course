import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect, useReducer } from 'react';
import { Product, NEWPRODUCTID} from "./components/product.js";
import { SelectedCategoryProductList, ProductCategories, allProductsCategory } from "./components/categories.js";

const initialProduct = {
  id: NEWPRODUCTID,
  title: "",
  description: "",
  price: "",
  image: "https://via.placeholder.com/150",
}

const initialProductState = { 
  mode: "show",
  data: initialProduct
}

// Active product
function activeProductReducer (state, event) {
  switch (event.action) {
    case "set": 
      return event.data
    case "new":
        return {
          mode: "edit",
          data: initialProduct
        }
    case "reset":
      return {
        mode: "show",
        data: null,
      };
    default:
      throw new Error("reducer: Unknown action type!");
  }
}

// Store
const FakeStoreProducts = ({storeApiUrl}) => {
  const [productCategories, setProductCategories] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  const [activeProductCategory, setActiveProductCategory] = useState("");
  const [activeProduct, activeProductDispatch] = useReducer(activeProductReducer, initialProductState);

  // Run once at start
  useEffect(() => {
    // Fetch categories
    fetch(`https://${storeApiUrl}/products/categories`)
    .then(res=>res.json())
    .then(json=>{

      // Own category for all products
      setProductCategories([allProductsCategory].concat(json));
    })
    .catch(err => {
      console.log(err)
      Error("Failed to fetch categories");
    });

    // Fetch products
    fetch(`https://${storeApiUrl}/products/`)
    .then(res=>res.json())
    .then(json=> {

      setAllProducts(json);
      // Use all products as default
      setActiveProductCategory(allProductsCategory);
    })
    .catch(err => {
      console.log(err)
      Error("Failed to fetch products");
    });

  }, []);

  // Handles product category change
  const getProductsInCategory = (e) => {
    e.preventDefault();
    const category = e.target.value;

    // Sanity
    if (category === undefined || category === null || category === "") return;
    setActiveProductCategory(category);
  }
  
  // Set product to activeProduct state
  const selectProduct = (product) => {
    activeProductDispatch({
      action: "set",
      data: {
        mode: "show",
        data: product
      }
    });
  }

  // Update product on the list
  const addProduct = (newProduct) => {
    const updatedProducts = [...allProducts, newProduct];
    setAllProducts(updatedProducts);
    selectProduct(newProduct);
  }

  // Update product on the list
  const updateProduct = (updatedProduct) => {
    const updatedProducts = allProducts.map(product => (product.id === updatedProduct.id)
       ? updatedProduct : product
    );
    setAllProducts(updatedProducts);
    selectProduct(updatedProduct);
  }

  // Delete product from list
  const deleteProduct = (productId) => {
    const updatedProducts = allProducts.filter(product => product.id === productId ? false : true); //WTF not first time
    setAllProducts(updatedProducts);
    activeProductDispatch({
      action: "new"
    });
  }

  return (
    <>
    <div id="fake-store-container" className="container">
      <div className="row">
        <h1>FakeStore</h1>
      </div>
      <div className="row mb-4 mt-2">
        <div className="col-sm-10">
          <ProductCategories 
            productCategories={productCategories} 
            getProductsInCategory={getProductsInCategory} />
        </div>
        <div className="col-sm-2">
          <button type="button" className="btn btn-primary" onClick={() => activeProductDispatch({action: "new"})}>Add product</button>
        </div>
      </div>
      <div className="row mb-4 mt-4">
        <div className="col-sm-6">
          <h2>Products</h2>
          <SelectedCategoryProductList 
            products={allProducts} 
            activeProductCategory={activeProductCategory} 
            selectProduct={selectProduct} />
        </div>
        <div className="col-sm-6">
          <Product 
            product={activeProduct} 
            addProduct={addProduct}
            updateProduct={updateProduct}
            deleteProduct={deleteProduct}
            productCategories={productCategories} />
        </div>
      </div>
    </div>
    </>
  );
}

function App() {
  return (
    <div className="App">
      <FakeStoreProducts storeApiUrl="fakestoreapi.com"/>
    </div>
  );
}

export default App;
