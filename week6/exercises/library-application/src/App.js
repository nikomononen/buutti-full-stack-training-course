import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import BookEdit from './components/bookEdit.js';
import BookList from './components/bookList.js';

const initialState = [
  {
    id: "ae7a4a9d-cdc2-47bf-b642-7a625d666d3c",
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
    read: true
 },
 {
    id: "1dfaccaa-46e7-4493-a24a-33fc4c9586ae",
    name: "REST API Design Rulebook",
    author: "Mark Masse",
    read: false
 }
];

const Library = function(props){
  const [bookList, setBookList] = useState(props.list);
  // Todo change to empty book
  const [selectedBook, setSelectedBook] = useState(bookList[0]);

  const saveChanges = (bookId, changes) => {
    const newList = bookList.map(book => (book.id === bookId ? Object.assign(book, changes) : book))
    setBookList(newList);
  }

  const deleteBook = (bookId) => {
    const deletedBookList = bookList.filter((book) => book.id !== bookId);
    setSelectedBook({
      id : uuidv4(),
      name: "",
      author: "",
      read: false
    });
    setBookList(deletedBookList);
  }
  /*
    react-dom.development.js:67 Warning: A component is changing a controlled input to be uncontrolled. 
    
    This is likely caused by the value changing from a defined to undefined, which should not happen. 
    Decide between using a controlled or uncontrolled input element for the lifetime of the component. More info: https://reactjs.org/link/controlled-components
    at input
    at label
    at form
    at BookEdit (http://localhost:3000/static/js/bundle.js:369:5)
    at div
    at div
    at div
    at Library (http://localhost:3000/static/js/bundle.js:101:89)
    at App 
  */
  // - pitäisikö olla setSelectedBook?
  // - vai tarviiko ollenkaan?
  // -Tarkista missä tarvitaan.
  // - Vaan inputtien muuuttiminen ei ollu tarkoitus vaan uusi state olisi siellä jos
  // paitsi kun valitaan
  //  - Alussa ja poistettaessa uus olio?
  //  - Renderöinnin väärin ymmärtäminen? millä saadaan aikaiseksi?
  //
  const addNewBook = (book) => {
    const newBook = {
      id : uuidv4(),
      name: book.name,
      author: book.author,
      read: book.read
    };
    bookList.push(newBook);
    setBookList([...bookList]);
  }

  const updateSelectedBook = (book) => {
    setSelectedBook(book);
    console.log(selectedBook);
  }
  
  return (
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <h1>Library</h1>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4">
          <h2>Books</h2>
          <BookList books={bookList} setSelectedBook={updateSelectedBook}/>
        </div>
        <div class="col-sm-8">
          <h2>Edit</h2>
          <BookEdit book={selectedBook} 
            saveChanges={saveChanges} 
            addNewBook={addNewBook} 
            deleteBook={deleteBook} 
            />
        </div>
      </div>

    </div>
  );
}

function App(props) {
  return (
    <Library list={initialState}/>
  );
}

export default App;
