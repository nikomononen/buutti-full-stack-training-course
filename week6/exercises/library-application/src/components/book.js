import React from "react";

const Book = ({book, onClick}) => {
    return (
        <div className="book" id={book.id}>
            <div class="mb-3" onClick={onClick}>
                <div class="title bold"><strong>{book.name}</strong></div>
                <div class="small"><em>{book.author}</em></div>
                <div class="small">{ book.read ? "Complete" : "Incomplete" }</div>
            </div>
        </div>
    )
}

export default Book;