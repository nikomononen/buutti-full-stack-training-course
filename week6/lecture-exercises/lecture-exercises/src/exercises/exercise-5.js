import React, { useState } from 'react';

const TextInputComponent = () => {
  const [inputString, setInputString] = useState("");
  const [visibleString, setVisibleString] = useState("");

  function formSubmitHandler(event) {
    setVisibleString(inputString);
    event.preventDefault();
  }

  function textChangeHandler(event) {
    setInputString(event.target.value);
  }

  return (
    <>
      <h1>{visibleString}</h1>
      <form onSubmit={formSubmitHandler}>
        <input type="text" value={inputString} onChange={textChangeHandler} />
        <button type="submit">Submit</button>
      </form>
    </>
  );
}

//---
import logo from './logo.svg';
import './App.css';
import React, { useState } from 'react';


function App() {
  return (
    <div className="App">
      <TextInputComponent />
    </div>
  );
}

export default App;
