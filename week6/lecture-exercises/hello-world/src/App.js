import logo from './logo.svg';
import './App.css';
const items = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

function App(props) {
  return (
    <div>
    <h1>{ props.title }</h1>
    <Assignment5 list={items}/>
    <h2>It is { new Date().getFullYear() }</h2>
    
    </div>
  );
}

function Assignment5(props) {
  const list = props.list.map((item, index) => {
    if (index % 2 ) {
      return <b key="{item}">{item}</b>
    } else {
      return <i key="{item}">{item}</i>
    }
  });
  
  return (
    <div>
      <h3>{list}</h3>
    </div>
  );
}


export default App;
