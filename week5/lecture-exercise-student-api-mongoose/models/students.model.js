import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    uuid: String,
    name: String,
    email: String,
});

export const Students = mongoose.model("Students", UserSchema);