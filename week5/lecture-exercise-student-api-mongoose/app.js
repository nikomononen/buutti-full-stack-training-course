import express from "express";
import studentRouter from "./routes/students.router.js";
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();

const PORT = 5000;

await mongoose.connect(
    "mongodb://127.0.0.1:27017/testDatabase",
    { 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        user: process.env.MONGODB_USER,
        pass:  process.env.MONGODB_PASSWORD
    }
);

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/students", studentRouter);

app.listen(PORT, () => {
    console.log(`Listening to ${PORT}`);
});