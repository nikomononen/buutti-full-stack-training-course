import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Owners } from "../models/owners.model.js";

// Authorization middleware
export const authorizationMiddleware = async (request, response, next) => {
    const authToken = request.get("authorization");
    if (authToken && authToken.startsWith("Bearer ")) {
        try {
            // Substring "Bearer " and verify token
            const token = authToken.substring(7);
            const decodedToken = jwt.verify(token, process.env.APP_SECRET);

            if (!decodedToken._id) {
                return response.status(403).json({
                    error: "Access token is invalid"
                });
            } else {
                request.authenticatedUser = await Owners.findById(decodedToken._id);
                next();
            }
        }
        catch (error) {
            switch (error.constructor) {
                case jwt.TokenExpiredError: {
                    return response.status(403).json({
                        error: error.message
                    });
                }
                case jwt.JsonWebTokenError: {
                    return response.status(401).json({
                        error: error.message
                    });
                }
                default: {
                    return response.status(500).json({
                        error: error.message
                    });
                }
            }
        }
    } else {
        return response.status(401).json({
            error: "Auth token missing"
        });
    }
};

/**
 * Login. Creates and returns JWT token on successful login
 */
export const login = async (request, response) => {
    const email = request.body.email;
    const password = request.body.password;
    const user = await Owners.findOne( { contact_email : email });

    if (user) {
        try {
            const passwordIsCorrect = await bcrypt.compare(password, user.password);
            if (passwordIsCorrect) {
                // Sign JWT token
                try {
                    const token = jwt.sign(
                        { 
                            _id: user._id 
                        },
                        process.env.APP_SECRET,
                        { expiresIn: "1h" }
                    );
                    response.status(200).json({
                        "status": "Access granted!",
                        "token": token
                    });
                } catch (error) {
                    // jwt.sign thrown error
                    response.status(500).json({
                        "error": error.message
                    });
                }
            }
            else {
                response.status(403).json({
                    "status": "Access denied!"
                });
            }
        }
        catch (error) {
            // bcrypt.compare thrown error
            response.status(500).json({
                "error": error.message
            });
        }
    } else {
        response.status(401).json({
            "status": "Access denied!"
        });
    }
};