import { Cars } from "../models/cars.model.js";
import { Owners } from "../models/owners.model.js";

export const getUsedCar = (req, res) => {
    Cars.findOne({id: req.params.id}).then(result => {
        if (result) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const postUsedCar = (req, res) => {
    const usedCar = new Cars({ ...req.body, owner_id: req.authenticatedUser.id });
    usedCar.save().then(result => {
        if (result) res.status(200).json(result);
        else res.status(500).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const searchUsedCar = async (req, res) => {
    const queryParams = req.query;
    const queryString = {};

    // price min & max
    if ("priceMin" in queryParams) {
        if ("priceMax" in queryParams) {
            queryString["price"] =  { $gte: queryParams.priceMin, $lte: queryParams.priceMax };
        } else {
            queryString["price"] =  { $gte: queryParams.priceMin };
        }
    } else if ("priceMax" in queryParams) {
        queryString["price"] =  { $lte: queryParams.priceMax };
    }

    // car model year min & max
    if ("yearMin" in queryParams) {
        if ("yearMax" in queryParams) {
            queryString["car_modelyear"] =  { $gte: queryParams.yearMin, $lte: queryParams.yearMax };
        } else {
            queryString["car_modelyear"] =  { $gte: queryParams.yearMin };
        }
    } else if ("yearMax" in queryParams) {
        queryString["car_modelyear"] =  { $lte: queryParams.yearMax };
    }

    // Make
    if ("car_make" in queryParams) {
        queryString["car_make"] = queryParams.car_make;
    }

    // Model
    if ("car_model" in queryParams) {
        queryString["car_model"] = queryParams.car_model;
    }

    // Owner state
    if ("owner_state" in queryParams) {
        try {
            const ownersInState = await Owners.find({ owner_state: queryParams.owner_state });
            queryString["owner_id"] = { $in: ownersInState };
        } catch(error) {
            res.status(500).json(error);
        }
    }

    Cars.find(queryString).then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};



