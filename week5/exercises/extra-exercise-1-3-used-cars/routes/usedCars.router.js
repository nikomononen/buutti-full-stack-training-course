import express from "express";
import * as UsedCarsController from "../controllers/usedCars.controller.js";
import { authorizationMiddleware } from "../controllers/authentication.controller.js";

const router = express.Router();

// Use JWT authentication middleware
router.use(authorizationMiddleware);

router.get("/usedCars/:id", UsedCarsController.getUsedCar);
router.post("/usedCars/", UsedCarsController.postUsedCar);
router.get("/usedCars/", UsedCarsController.searchUsedCar);

export default router;