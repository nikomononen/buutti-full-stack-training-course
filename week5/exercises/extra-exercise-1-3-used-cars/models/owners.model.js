import mongoose from "mongoose";

const OwnersSchema = new mongoose.Schema({
    password: String,
    owner_firstname: String,
    owner_surname: String,
    contact_email: String,
    owner_address: String,
    owner_state: String,
});

export const Owners = mongoose.models.owners || mongoose.model("owners", OwnersSchema);