import express from "express";
import authenticationRouter from "./routes/authentication.route.js";
import usedCarsRouter from "./routes/usedCars.router.js";
import { Cars } from "./models/cars.model.js";
import { Owners } from "./models/owners.model.js";
import mongoose from "mongoose";
import dotenv from "dotenv";
import fs from "fs";

dotenv.config();

const PORT = 5000;

await mongoose.connect(
    "mongodb://127.0.0.1:27017/testDatabase",
    { 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        user: process.env.MONGODB_USER,
        pass: process.env.MONGODB_PASSWORD
    }
);

async function importJson(filename) {
    const contents = JSON.parse(fs.readFileSync(filename, { encoding:"utf-8" }));
    contents.forEach(async (object) => {
        try {
            let owner = await Owners.findOne({ email: object.contact_email });
            if (owner === null) {
                owner = new Owners(object);
                await owner.save();
            } 
            const car = new Cars({ ...object, owner_id: owner.id });
            await car.save();
        } catch(error){
            console.log(error);
        }
    });
}

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Logger middleware
const logger = (request, response, next) => {
    const isoDateNow = new Date().toISOString();
    const fullUrl = request.protocol + "://" + request.get("host") + request.originalUrl;
    const requestParamsString = Object.keys(request.params).length > 0 ? "/ Request params: " + JSON.stringify(request.params) : "";
    const requestDataString = request.body ? "/ Request body " + JSON.stringify(request.body) : "";
    response.on("finish", () => {
        console.info(`[${isoDateNow}] ${request.method} ${fullUrl} / ${response.statusCode} ${response.statusMessage} ${requestParamsString} ${requestDataString}`);
    });
    next();
};

// Use request logging
app.use(logger);

// Login 
app.use(authenticationRouter);

// Routes
app.use(usedCarsRouter);

// Start app
app.listen(PORT, () => {
    const importFile = process.argv[2];
    if (importFile !== undefined) importJson(importFile);
    console.log(`Listening to ${PORT}`);
});