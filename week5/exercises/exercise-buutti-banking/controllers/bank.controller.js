import { User } from "../models/user.model.js";
import { FundRequest } from "../models/fundrequest.model.js";

/**
 *  Get balance of the given user
 *  /bank/:user_id/balance
 */
export const getBalance = async (req, res) => {
    const user = await User.findById(req.params.user_id);
    if (user) {
        res.status(200).json({ balance: user.balance });
    } 
    else {
        res.status(404).json({ 
            error: "User not found"
        });
    }
};

/**
 *  Deposit given amount to user account
 */
export const deposit = async(req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const amount = Number(req.body.amount);

    if (amount === undefined || isNaN(amount) || amount < 0) {
        validationErrors.push("Amount must be given and to be positive number");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const user = await User.findById(userId);

    if (user === null) {
        res.status(404).json({ 
            error: "User not found"
        });
        return;
    }

    user.balance += amount;
    user.save().then(result => {
        res.status(200).json({ balance: result.balance });
    }).catch(error => {
        res.status(500).json(error);
    });
};

/**
 *  Withdraw given amount from user account. 
 *  Validates input and returns error message if not enough money on the account
 */
export const withdraw = async(req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const amount = Number(req.body.amount);

    if (amount === undefined || isNaN(amount) || amount < 0) {
        validationErrors.push("Amount must be given and be positive number");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const user = await User.findById(userId);

    if (user === null) {
        res.status(404).json({ 
            error: "User not found"
        });
        return;
    }

    const newAmount = user.balance - amount;
    if (newAmount >= 0) {
        user.balance = newAmount;
        user.save().then(result => {
            res.status(200).json({ balance: result.balance });
        }).catch(error => {
            res.status(500).json(error);
        });
    } 
    else {
        res.status(400).json({ 
            error: "Not enough balance"
        });
    }
};

/**
 *  Transfer given amount from user account to another user. 
 *  Validates input and returns error message if not enough money on the account
 */
export const transfer = async(req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const recipientUserId = req.body.recipient_id;
    const amount = Number(req.body.amount);

    if (recipientUserId === undefined || recipientUserId.length < 1) {
        validationErrors.push("Recipient id must be given");
    }

    if (amount === undefined || isNaN(amount) || amount < 0) {
        validationErrors.push("Amount must be given and be positive number");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const user = await User.findById(userId);

    if (user === null) {
        res.status(404).json({ 
            error: "User not found"
        });
        return;
    }

    const recipientUser = await User.findById(recipientUserId);
    if (recipientUser === null) {
        res.status(404).json({ 
            error: "Recipient not found"
        });
        return;
    }

    const userNewBalance = user.balance - amount;
    const recipientNewBalance = recipientUser.balance + amount;

    if (userNewBalance < 0) {
        res.status(400).json({ 
            error: "Not enough balance"
        });
        return;
    }

    try {    
        user.balance = userNewBalance;
        await user.save();

        recipientUser.balance = recipientNewBalance;
        await recipientUser.save();

        return res.status(200).json({ balance: userNewBalance });
    }
    catch(error) {
        res.status(500).json(error);
    }
};

/**
 * Gets all fund requests for authenticated user
 */
export const getFundRequests = async (req, res) => {
    FundRequest.find({ target_id: req.authenticatedUser.id }).then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

/**
 * Creates new fund request from authenticated user
 * to given user(s). If multiple user is given, the amount is split for the
 * number of users
 */
export const createFundRequest = async(req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const targetUserId = req.body.id;
    const amount = Number(req.body.amount);

    if (targetUserId === undefined) {
        validationErrors.push("User id must be given");
    } 

    if (amount === undefined || isNaN(amount) || amount < 0) {
        validationErrors.push("Amount must be given and be positive number");
    }

    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const user = await User.findById(userId);

    if (user === null) {
        res.status(404).json({ 
            error: "User not found"
        });
        return;
    }

    // If target id is not array, make it 
    const targetUserIdArray = Array.isArray(targetUserId) ? targetUserId : [targetUserId];

    // Create fund requests for each user
    const fundRequestsArray = [];
    const splitAmount = amount / targetUserIdArray.length;

    for (const targetUserId of targetUserIdArray){
        const targetUser = await User.findById(targetUserId);
        if (targetUser === null) {
            res.status(404).json({ 
                error: `Target user (id: ${targetUserId}) not found`
            });
            return;
        }
        
        const fundRequest = new FundRequest({
            user_id: user.id,
            target_id: targetUser.id,
            amount: splitAmount
        });
    
        fundRequestsArray.push(fundRequest);
    }

    // Save all fund requests
    const fundRequestIds = [];
    try {
        for (const fundRequest of fundRequestsArray){
            const result = await fundRequest.save();
            fundRequestIds.push(result.id);
        }
        return res.status(200).json({ ids: fundRequestIds });
    } catch(error){
        res.status(500).json(error);
    }
};

/**
 * Accepts fund requests from authenticated user
 */
export const acceptFundRequest = async(req, res) => {
    const validationErrors = [];
    const userId = req.authenticatedUser.id;
    const fundRequestId = req.body.id;

    if (fundRequestId === undefined || fundRequestId.length < 1) {
        validationErrors.push("Fund request id must be given");
    }
    if (validationErrors.length > 0 ) {
        res.status(400).json({ 
            error: validationErrors
        });
        return;
    }

    const fundRequest = await FundRequest.findById(fundRequestId);

    if (fundRequest === null) {
        res.status(404).json({ 
            error: "Fund request not found"
        });
        return;
    }

    // Logged in user is target_id from whom the amount is requested
    const user = await User.findById(userId);
    if (user === null) {
        res.status(404).json({ 
            error: "User user not found"
        });
    }

    if (!fundRequest.target_id.equals(user._id)) {
        res.status(401).json({ 
            error: "Fund request is not for authenticated user"
        });
        return;
    }
    
    // Creator of the fund request is who gets the money
    const targetUser = await User.findById(fundRequest.user_id);
    if (targetUser === null) {
        res.status(404).json({ 
            error: "Target user not found"
        });
    }

    try {    
        const newBalance = user.balance - fundRequest.amount;
        if (newBalance < 0) {
            res.status(400).json({ 
                error: "Not enough balance"
            });
            return;
        }

        user.balance = newBalance;
        targetUser.balance = targetUser.balance + fundRequest.amount;

        await user.save();
        await targetUser.save();
        await fundRequest.remove();

        return res.status(200).json({ balance: user.balance });
    }
    catch(error) {
        res.status(500).json(error);
    }
};