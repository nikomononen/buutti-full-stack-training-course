import mongoose from "mongoose";
import bcrypt from "bcrypt";
const bcryptHashRounds = 10;

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name must be given"],
        minLength: [1, "Name must be at least 1 character long"]
    },
    password: {
        type: String,
        required: [true, "Password must be given"],
        minLength: [1, "Password must be at least 1 character long"]
    },
    balance: {
        type: Number,
        required: [true, "Amount must be given"],
        min: [0, "Balance must be positive number"]
    }
});

// Hash password before storing
UserSchema.pre("save", async function (next) {
    // eslint-disable-next-line no-unused-vars
    this.password = await bcrypt.hash(this.password, bcryptHashRounds);
    next();
});

export const User = mongoose.models.user || mongoose.model("user", UserSchema);