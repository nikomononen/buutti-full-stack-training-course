import mongoose from "mongoose";
const Schema = mongoose.Schema, ObjectId = Schema.ObjectId;

const FundRequestSchema = new mongoose.Schema({
    user_id: {
        type: ObjectId,
        required: [true, "User id must be given"]
    },
    target_id: {
        type: ObjectId,
        required: [true, "Target user id must be given"]
    },
    amount: {
        type: Number,
        required: [true, "Amount must be given"],
        min: [0, "Amount must be positive number"]
    }
});

export const FundRequest = mongoose.models.fundRequest || mongoose.model("fundRequest", FundRequestSchema);