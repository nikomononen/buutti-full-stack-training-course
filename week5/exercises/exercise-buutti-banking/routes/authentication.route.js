import express from "express";
import * as AuthenticationController from "../controllers/authentication.controller.js";

const router = express.Router();

router.post("/bank/login", AuthenticationController.login);

export default router;