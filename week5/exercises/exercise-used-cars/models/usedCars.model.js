import mongoose from "mongoose";
/**
 {
     "id":1,
     "owner_firstname":"Marylène",
     "owner_surname":"Deekes",
     "contact_email":"ldeekes0@newyorker.com",
     "owner_address":"56 Roth Circle",
     "owner_state":"Texas",
     "car_make":"Ford",
     "car_model":"LTD Crown Victoria",
     "car_modelyear":1989,
     "price":71990.31
 }
*/
const UsedCarsSchema = new mongoose.Schema({
    id: Number,
    owner_firstname: String,
    owner_surname: String,
    contact_email: String,
    owner_address: String,
    owner_state: String,
    car_make: String,
    car_model: String,
    car_modelyear: Number,
    price: Number,
});

export const UsedCars = mongoose.model("user_cars", UsedCarsSchema);