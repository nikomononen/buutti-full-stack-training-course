import { UsedCars } from "../models/usedCars.model.js";

export const getUsedCar = (req, res) => {
    UsedCars.find({id: req.params.id}).then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};

export const postUsedCar = (req, res) => {
    const usedCar = new UsedCars(req.body);
    usedCar.save().then(result => {
        if (result.length > 0) res.status(200).json(result);
        else res.status(404).json(result);
    }).catch(error => {
        res.status(500).json(error);
    });
};



