import express from "express";
import usedCarsRouter from "./routes/usedCars.router.js";
import { UsedCars } from "./models/usedCars.model.js";
import mongoose from "mongoose";
import dotenv from "dotenv";
import fs from "fs";

dotenv.config();

const PORT = 5000;

await mongoose.connect(
    "mongodb://127.0.0.1:27017/testDatabase",
    { 
        useNewUrlParser: true, 
        useUnifiedTopology: true,
        user: process.env.MONGODB_USER,
        pass:  process.env.MONGODB_PASSWORD
    }
);

async function importJson(filename, schema) {
    const contents = JSON.parse(fs.readFileSync(filename, {encoding:"utf-8"}));
    try {
        await schema.insertMany(contents);
        console.log(`Imported ${contents.length} objects from ${filename}`);
    } catch(error){
        console.log(error);
        process.exit(1);
    }
    
}

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/usedCars", usedCarsRouter);

app.listen(PORT, () => {
    const importFile = process.argv[2];
    if (importFile !== undefined) importJson(importFile, UsedCars);
    console.log(`Listening to ${PORT}`);
});