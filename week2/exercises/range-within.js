/**
   Range within
   Write a program that takes in any two numbers from the command line, start and end. 
   The program creates and prints an array filled with numbers from start to end.

   Examples:
   node .\createRange.js 1 5 -> [1, 2, 3, 4, 5]
   node .\createRange.js -5 -1 -> [-5, -4, -3, -2, -1]
   node .\createRange.js 9 5 -> [9, 8, 7, 6, 5]
   Note the order of the values. When start is smaller than end , the order is ascending and when start is greater than end , order is descending.
 */

/**
 * Creates an array of numbers withing given range.
 * When a is smaller than b, the order is ascending and when a is greater than b, order is descending.
 *
 * @param {number} a - range min
 * @param {number} b - range max
 * @returns {Array.<number>} 
 */
function createRange(a, b){
    const array = [];
    // Ascending
    if (b - a > 0){
        for(let i = a; i <= b; i++){
            array.push(i);
        }
    } else {
        // Descending
        for(let i = a; i >= b; i--){
            array.push(i);
        }
    }
    return array;
}

const rangeMin = parseInt(process.argv[2]);
const rangeMax = parseInt(process.argv[3]);
if (!isNaN(rangeMin) && !isNaN(rangeMax)){
    console.log(createRange(rangeMin, rangeMax));
}