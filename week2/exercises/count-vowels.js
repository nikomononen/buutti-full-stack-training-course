/**
    4. Count vowels
    Return the number (count) of vowels in the given string. Let's consider a, e, i, o, u, y as vowels in this exercise.
    getVowelCount('abracadabra') //  
 */
const vowelsArray = ["a", "e", "i", "o", "u", "y"];

/**
 * Counts vowels in the given string
 * @param {string} string
 * @return {number} count of vowels in the string
 */
function getVowelCount(string){
    return string.split("").reduce((vowels, letter) => {
        if (vowelsArray.includes(letter)) vowels += 1;
        return vowels;
    }, 0);
}

console.log(getVowelCount("abracadabra")); // 5
console.log(getVowelCount("abba")); // 2