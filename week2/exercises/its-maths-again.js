/**
    7. It's Maths again!
    Define a number n that is larger than 0, for example n = 3 
    Create a function that given parameter n finds the number of steps it takes to reach number 1 (one) using the following process
    - If n is even, divide it by 2
    - If n is odd, multiply it by 3 and add 1

    Example:
    For n = 3 the process would be following
    1 is odd → n = 3 * 3 + 1 = 10
    2: 10 is even → n = 10 / 2 = 5
    3: 5 is odd → n = 3 * 5+1 = 16
    4: 16 is even → n = 16 / 2 = 8
    5: 8 is even → n = 8 / 2 = 4
    6: 4 is even → n = 4 / 2 = 2
    7: 2 is even → n = 2 / 2 = 1
    And finally we reached nr. 1 after 7 step
*/

/**
 * Checks if given number n is odd or even and applies corresponding equation
 * 
 * @param {number} n
 * @returns {Object>} Object of n and step
 */
function step(n){
    if (n <= 1) return {number: n, step: ""};
    let step;
    if (n % 2 === 0) {
        const prevN = n;
        n = n / 2;
        step = `${prevN} is even → n = ${prevN} / 2 = ${n}`;
    } else {
        const prevN = n;
        n = n * 3 + 1;
        step = `${prevN} is odd → n = 3 * ${prevN} + 1 = ${n}`;
    }
    return {
        number: n,
        step: step
    };
}

/**
 * Finds steps to reach number 1 with algorithm @see {@link step}.
 * 
 * @param {number} n
 * @returns {Array.<Object>} Array of Objects with number and step
 */
function findStepsToReachNumber(n){
    const array = [];
    while(n > 1){
        const stp = step(n);
        n = stp.number;
        array.push(stp);
    }
    return array;
}

/**
 * Checks if given number n is odd or even and applies corresponding equation
 * 
 * @param {number} n
 * @returns {Array.<{Object}>} Array of Object with n and step
 */
function stepRecursive(n, array) {
    if (n <= 1) return array;
    let step;
    if (n % 2 === 0) {
        const prevN = n;
        n = n / 2;
        step = `${prevN} is even → n = ${prevN} / 2 = ${n}`;
    } else {
        const prevN = n;
        n = n * 3 + 1;
        step = `${prevN} is odd → n = 3 * ${prevN} + 1 = ${n}`;
    }
    // Add to array
    array.push({
        number: n,
        step: step
    });
    return stepRecursive(n, array);
}

/**
 * Finds steps to reach number 1 with algorithm @see {@link stepRecursive}. 
 * 
 * @param {number} n
 * @returns {Array.<{Object}>} Array of Object with n and step
 */
function findStepsToReachNumberRecursive(n){
    const array = [];
    return stepRecursive(n, array);
}

// With while loop
const steps = findStepsToReachNumber(3);
const stepsCount = steps.reduce((accumulator, stepObject, index) => {
    console.log(`${index + 1}: ${stepObject.step}`);
    return accumulator += 1;
},0);
console.log(`And finally we reached nr. 1 after ${stepsCount} step`);
console.log("-----");

// Same with recursive
const stepsRecursive = findStepsToReachNumberRecursive(5);
const stepsCountRecursive = stepsRecursive.reduce((accumulator, stepObject, index) => {
    console.log(`${index + 1}: ${stepObject.step}`);
    return accumulator += 1;
},0);
console.log(`And finally we reached nr. 1 after ${stepsCountRecursive} step`);
