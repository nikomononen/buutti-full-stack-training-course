/**
 * Count sheeps
 * Create a program that takes in a number from the command line, for example node .\countSheep.js 3 and prints a string "1 sheep...2 sheep...3 sheep..."
 */

/**
 * Print n amount of sheeps to count
 * @param  {number} sheepCount
 */
function printSheepCount(sheepCount){
    const array = [];
    for (let i = 1; i <= sheepCount; i++){
        array.push(`${i} sheep...`);
    }
    console.log(array.join(""));
}

const numberFromCommandLine = parseInt(process.argv[2]);
if (!isNaN(numberFromCommandLine) && numberFromCommandLine > 0) {
    printSheepCount(numberFromCommandLine);
}