/** 
 * Randomize order of array
 * const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
 * Create a program that every time you run it, prints out an array with differently randomized order of the array above.
 * Example:
 * node .\randomizeArray.js -> [5, 4, 18, 32, 8, 6, 2, 25, 14, 10]
 */
const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

/**
 * Return a random number withing given range.
 *  
 * @param {number} min
 * @param {number} max
 * @returns {number} random number within given range
 */
function getRandomNumber(min, max){
    const randomNumber = Math.floor(Math.random() * (max - min +1)) + min;
    return randomNumber;
}

/**
 * Shuffles items randomly in the given array using Fisher–Yates shuffle.
 *
 * @param {Array.<any>}
 * @returns {Array.<any>} sorted array
 */
function shuffleArray(array){
    const shuffledArray = array;
    for (let i = array.length - 1; i > 0; i--){
        const randomizedIndex = getRandomNumber(0, i);
        if (randomizedIndex !== i){
            const tmp = shuffledArray[i];
            shuffledArray[i] = shuffledArray[randomizedIndex];
            shuffledArray[randomizedIndex] = tmp;
        }
    }
    return shuffledArray;
}

// Print some shuffled arrays
for(let i = 0; i < 10; i++){
    console.log(shuffleArray(array));
}