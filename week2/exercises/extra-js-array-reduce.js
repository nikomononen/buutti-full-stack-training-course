/**
 *  Array Reduce Exercises
 *  @see {@link https://coursework.vschool.io/array-reduce-exercises/}
 */

// 1) Turn an array of numbers into a total of all the numbers
function total(array) {
    return array.reduce((total, number) => total += number);
}

console.log(total([1,2,3])); // 6

// 2) Turn an array of numbers into a long string of all those numbers.
function stringConcat(array) {
    return array.reduce((string, number) => string += number.toString(), "");
}

console.log(stringConcat([1,2,3])); // "123"

// 3) Turn an array of voter objects into a count of how many people voted 
function totalVotes(array) {
    return array.reduce((voters, voter) => {
        if (voter.voted) return voters + 1;
        else return voters;
    }, 0);
}

const voters = [
    {name:"Bob" , age: 30, voted: true}, {name:"Jake" , age: 32, voted: true}, {name:"Kate" , age: 25, voted: false},
    {name:"Sam" , age: 20, voted: false}, {name:"Phil" , age: 21, voted: true}, {name:"Ed" , age:55, voted:true},
    {name:"Tami" , age: 54, voted:true}, {name: "Mary", age: 31, voted: false}, {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true}, {name: "Jeff", age: 30, voted: true}, {name: "Zack", age: 19, voted: false}
];

console.log(totalVotes(voters)); // 7

// 4) Given an array of all your wishlist items, figure out how much it would cost to just buy everything at once
function shoppingSpree(array) {
    return array.reduce((total, item) => total += item.price, 0);
}

const wishlist = [
    { title: "Tesla Model S", price: 90000 },
    { title: "4 carat diamond ring", price: 45000 },
    { title: "Fancy hacky Sack", price: 5 },
    { title: "Gold fidgit spinner", price: 2000 },
    { title: "A second Tesla Model S", price: 90000 }
];

console.log(shoppingSpree(wishlist)); // 227005

// 5) Given an array of arrays, flatten them into a single array
function flatten(array) {
    return array.reduce((result, arr) => result.concat(arr)); 
}
 
const arrays = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];

console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];

/*
   6) Given an array of potential voters, return an object representing the results of the vote 
   Include how many of the potential voters were in the  ages 18-25, how many from 26-35, how many from 36-55, 
   and how many of each of those age ranges actually voted.
 */
const voters2 = [
    {name:"Bob" , age: 30, voted: true},
    {name:"Jake" , age: 32, voted: true},
    {name:"Kate" , age: 25, voted: false},
    {name:"Sam" , age: 20, voted: false},
    {name:"Phil" , age: 21, voted: true},
    {name:"Ed" , age:55, voted:true},
    {name:"Tami" , age: 54, voted:true},
    {name: "Mary", age: 31, voted: false},
    {name: "Becky", age: 43, voted: false},
    {name: "Joey", age: 41, voted: true},
    {name: "Jeff", age: 30, voted: true},
    {name: "Zack", age: 19, voted: false}
];

/**
 * Function to increase given object attribute value by 1
 *
 * @param  {Object} object - Object which attribute to be increased
 * @param  {string} attribute - Attribute to increase
 */

function increaseObjectAttributeCount(object, attribute){
    if (attribute in object) object[attribute] += 1;
    else object[attribute] = 1;
}
// eslint-disable-next-line no-unused-vars
function voterResults2(array) {
    return array.reduce((votersObject, voter) => {
        if(voter.age >= 18 && voter.age <= 25) {
            if(voter.voted) increaseObjectAttributeCount(votersObject, "numYoungVotesPeople");
            increaseObjectAttributeCount(votersObject, "numYoungPeople");
        } else if (voter.age >= 26 && voter.age <= 35){
            if(voter.voted) increaseObjectAttributeCount(votersObject, "numMidVotesPeople");
            increaseObjectAttributeCount(votersObject, "numMidsPeople");
        } else if (voter.age >= 36 && voter.age <= 55){
            if(voter.voted) increaseObjectAttributeCount(votersObject, "numOldVotesPeople");
            increaseObjectAttributeCount(votersObject, "numOldsPeople");
        }
        return votersObject;
    }, new Object());
}
const votersResultObject =  {
    numYoungVotesPeople: 0,
    numYoungPeople: 0,
    numMidVotesPeople: 0,
    numMidsPeople: 0,
    numOldVotesPeople: 0,
    numOldsPeople: 0
};
function voterResults(array) {
    return array.reduce((votersObject, voter) => {
        if(voter.age >= 18 && voter.age <= 25) {
            if(voter.voted) votersObject.numYoungVotesPeople += 1;
            votersObject.numYoungPeople += 1;
        } else if (voter.age >= 26 && voter.age <= 35){
            if(voter.voted) votersObject.numMidVotesPeople += 1;
            votersObject.numMidsPeople += 1;
        } else if (voter.age >= 36 && voter.age <= 55){
            if(voter.voted) votersObject.numOldVotesPeople += 1;
            votersObject.numOldsPeople += 1;
        }
        return votersObject;
    }, votersResultObject);
}
console.log(voterResults(voters2)); // Returned value shown below:
/*
{
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numYoungPeople: 4,
  numYoungVotes: 1,
  numOldVotesPeople: 3,
  numOldsPeople: 4
}
*/

/*
    ... Ja näillä jatkaa (Lähde: https://gist.github.com/TessMyers/a252520dd9a8fe68f8e5):
*/

/*
   1. Write a function that takes a string and returns an object representing the character count for each letter. 
   Use .reduce to build this object. ex. countLetters('abbcccddddeeeee') // => {a:1, b:2, c:3, d:4,e:5}
 */
const countLetters = function(string){
    const array = string.split("");
    return array.reduce((obj, char) => {
        increaseObjectAttributeCount(obj, char);
        return obj;
    }, new Object());
};

console.log(countLetters("abbcccddddeeeee")); // => {a:1, b:2, c:3, d:4, e:5}

/**
 * 2. Write a function that takes a string and a target, and returns true or false if the target is present in
 * the string. Use .reduce to acomplish this. 
 * Examples:
 * - isPresent('abcd', 'b') // => true ex.
 * - isPresent('efghi', 'a') //=> false
 * 
 * @param {string} string
 * @param {string} target - Character to check if is present in given string
 * @returns {boolean} wether the character is present in the string or not
 */
const isPresent = function(string, target) {
    const array = string.split("");
    return array.reduce((charIsPresent, char) => {
        if(!charIsPresent) {
            if (char === target) return true;
        }
        return charIsPresent;
    }, false);
};
const isPresent2 = function(string, target) {
    return string.split("").reduce((charIsPresent, char) => !charIsPresent ? char === target : charIsPresent, false);
};
console.log(isPresent("abcd", "b"));  // true
console.log(isPresent("efghi", "a")); // false
console.log(isPresent2("abcd", "d"));  // true
console.log(isPresent2("abcd", "2"));  // true

/**
 * 3. Write a function decode that will take a string of number sets and decode it using the following 
 * rules:
 * When each digit of each set of numbers is added together, the resulting sum is the ascii code for a single letter.
 * Convert each set of numbers into a letter and discover the secret message!
 *
 * Try using map and reduce together to accomplish this task.
 * ex. decode("361581732726247521644353 4161492813593986955 84654117917337166147521") //
 * => "abc"
 *
 * ex. decode("584131398786538461382741 444521974525439455955
 * 71415168525426614834414214 353238892594759181769 48955328774167683152
 * 77672648114592331981342373 5136831421236 83269359618185726749 2554892676446686256
 * 959958531366848121621517 4275965243664397923577 616142753591841179359
 * 121266483532393851149467 17949678591875681") // => "secret-message"
 * 
 * @param {string} string - String with sets of digits to decode
 * @returns {string} decoded string
 */
const decode = function(string){
    const decodedAsciiString = string.split(" ").map(digitsString => {
        // Splits digits and reduce the sum of digits. Then convert it to ascii
        const digitsArray = digitsString.split("");
        const sumOfDigits = digitsArray.reduce((sum, digit) => sum = sum + parseInt(digit), 0);
        return String.fromCharCode(sumOfDigits);
    }).join("");
    return decodedAsciiString;
};

console.log(decode("361581732726247521644353 4161492813593986955 84654117917337166147521")); // => "abc"
console.log(decode("584131398786538461382741 444521974525439455955 71415168525426614834414214 353238892594759181769 48955328774167683152 77672648114592331981342373 5136831421236 83269359618185726749 2554892676446686256 959958531366848121621517 4275965243664397923577 616142753591841179359 121266483532393851149467 17949678591875681")); 
// => "secret-message"

/*
  Extra credit exercise from:
  @see {@link https://coursework.vschool.io/array-reduce-exercises/}
 */
import axios from "axios";
const gitlabRepositoryId = 33550037; // https://gitlab.com/NikoMononen/buutti-full-stack-training-course
const commitsSinceDate  = new Date("2022-02-14 00:00:00");
const commitsUntil  = new Date("2022-02-15 23:59:59");

axios.get(`https://gitlab.com/api/v4/projects/${gitlabRepositoryId}/repository/commits`, {
    // Request headers
    headers: {
        //"PRIVATE-TOKEN": "<your_access_token>",
        "Content-Type": "application/json"
    },
    // Request params eg. /commits?since=&until=?ref_name=main
    params: {
        since: commitsSinceDate.toISOString(),
        until: commitsUntil.toISOString(),
        ref_name: "main"
    }
}).then(response => { // Success
    if (response.status === 200){
        const responseJSON = response.data;
        // Printout commits with reduce
        const commitsCount = responseJSON.reduce((commits, commit) => {
            const commitDate= new Date(commit.created_at);
            console.log(`[${commit.short_id}]{${commitDate.toLocaleString()}} ${commit.title}`);
            return commits += 1;
        }, 0);
        console.log(`There is total of ${commitsCount} commits since ${commitsSinceDate.toLocaleString()} until ${commitsUntil.toLocaleString()}.`);
    } else {
        console.log("HTTP response code is not 200 OK");
    }
}).catch(function (error) { // Error
    console.log(error);
}).then(function (){ // Run always
    console.log("Thanks for using gitlab!");
});