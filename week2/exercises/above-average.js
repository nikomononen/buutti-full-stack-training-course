/**
    3. Above average
    Create a function that takes in an array and returns a new array of values that are above average.
    aboveAverage([1, 5, 9, 3]) // outputs an array that has values greater than 4.5
*/

/**
 * Returns average of the array
 * 
 * @param {Array.<number>} array
 * @returns array average
 */
function average(array){
    return array.reduce((acc, num) => acc = acc + num, 0) / array.length;
}

/**
 * Returns new array where all numbers are greater than the average
 * 
 * @param {Array.<number>} array
 * @returns {Array.<number>}
 */
function aboveAverage(array){
    const avg = average(array);
    return array.filter((num) => {
        return num > avg;
    });
}

console.log(aboveAverage([1, 5, 9, 3, 4, 10]));