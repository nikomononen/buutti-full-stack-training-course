/**
    2. Likes
    Create a "like" function that takes in a array of names. Depending on a number of names (or length of the array)
    the function must return strings as follows:
    - likes([]); // "no one likes this"
    - likes(["John"]) // "John likes this"
    - likes(["Mary", "Alex"]) // "Mary and Alex like this"
    - likes(["John", "James", "Linda"]) // "John, James and Linda like this"
    - likes(["Alex", "Linda", "Mark", "Max"]) // must be "Alex, Linda and 2 others
    - For 4 or more names, "2 others" simply increases.
 */
/**
 * Returns formatted likes string for the given array
 * 
 * @param {Array.<string>} array - Array of users
 * @returns {string} formatted string 
 */
function likes(array){
    switch(array.length) {
        case 0: {
            return "No one likes this";
        }
        case 1: {
            return `${array[0]} likes this`;
        }
        case 2: {
            return `${array[0]} and ${array[1]} like this`;
        }
        case 3: {
            return `${array[0]}, ${array[1]} and ${array[2]} like this`;
        }
        default: {
            return `${array[0]}, ${array[1]} and ${array.length-2} others like this`;
        }
    }
}

console.log(likes([])); // "no one likes this"
console.log(likes(["John"])); // "John likes this"
console.log(likes(["Mary", "Alex"])); // "Mary and Alex like this"
console.log(likes(["John", "James", "Linda"])); // "John, James and Linda like this"
console.log(likes(["Alex", "Linda", "Mark", "Max"])); // must be "Alex, Linda and 2 others
console.log(likes(["Alex", "Linda", "Mark", "Max", "Johan"])); // must be "Alex, Linda and 3 others
console.log(likes(["Alex", "Linda", "Mark", "Max", "Johan", "Sebastian"])); // must be "Alex, Linda and 4 others