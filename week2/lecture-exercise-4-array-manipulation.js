/*
   Exercise 4: Array manipulation
   const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];
  
   From the elements of this array:
   1. create a new array with only the numbers that are divisible by three.
   2. Create a new array from original array (arr), where each number is multiplied by 2
   3. Sum all of the values in the array using the array method reduce
   console.log the result after each step
 */
const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

// 1. Divisible by 3
const numbersDivisibleByThreeArray = arr.filter(num => num % 3 === 0);
console.log(numbersDivisibleByThreeArray);

// 2. Each number in array multiplied by 2
const numbersMultipliedByTwoArray = arr.map(num => num * 2);
console.log(numbersMultipliedByTwoArray);

// 4: Sum of all values in the array
const numbersSum = arr.reduce((accumulator, num) => accumulator += num, 0);
console.log(`Sum of all the numbers in arr is: ${numbersSum}`);
