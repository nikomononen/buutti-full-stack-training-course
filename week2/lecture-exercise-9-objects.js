/*
const student = {
    className: "7B",
    mathGrade: 4, // 4-10
    finnishGrade: 8,
    historyGrade: 6,
    calculateAverage: function() {
        return (this.mathGrade + this.finnishGrade + this.historyGrade) / 3;    
    }
};
*/
function Student(className, mathGrade, finnishGrade, historyGrade){
    this.className = className, // 7A -> 9A
    this.mathGrade = mathGrade, // 4-10
    this.finnishGrade = finnishGrade,
    this.historyGrade = historyGrade,
    this.calculateAverage = function() {
        return Math.floor((this.mathGrade + this.finnishGrade + this.historyGrade) / 3);    
    };
}
Student.prototype.finishYear = function() {
    let currentClassNumber = parseInt(this.className[0]);
    if(currentClassNumber < 9) {
        currentClassNumber += 1;
        this.className = currentClassNumber + this.className[1];
    } else {
        console.log("Graduated!!");
    }
};
const matti = new Student("7A", 4, 5, 7);
const teppo = new Student("8C", 8, 8, 8);

console.log(`Matti average grade is ${matti.calculateAverage()}.`);
console.log(`Matti is at class ${matti.className}`);

for (let i = 0; i < 4; i++){
    console.log(`Teppo is at class ${teppo.className}`);
    teppo.finishYear();
}


/*
    Extra: implement same as class
*/
class StudentClass {
    constructor(className, mathGrade, finnishGrade, historyGrade){
        this.className = className; // 7A -> 9A
        this.mathGrade = mathGrade; // 4-10
        this.finnishGrade = finnishGrade;
        this.historyGrade = historyGrade;
    }
    calculateAverage = function() {
        return Math.floor((this.mathGrade + this.finnishGrade + this.historyGrade) / 3);    
    };
    finishYear = function() {
        let currentClassNumber = parseInt(this.className[0]);
        if(currentClassNumber < 9) {
            currentClassNumber += 1;
            this.className = currentClassNumber + this.className[1];
        } else {
            console.log("Graduated!!");
        }
    };
}
const seppo = new StudentClass("7D", 9, 9, 9);

console.log(`Seppo average grade is ${seppo.calculateAverage()}.`);

for (let i = 0; i < 4; i++){
    console.log(`Seppo is at class ${seppo.className}`);
    seppo.finishYear();
}