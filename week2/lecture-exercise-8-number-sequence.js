/*
   Exercise 8: number sequence
   Create a function, takes an number n as parameter, and
   produces an array of numbers, n being equal to the
   length of the array, that follows this sequence:
   const arr = [1, 1, 2, 3, 5, 8, 13, 21, 34, …];
 */

/**
 * Creates array of fibonacci numbers starting from n >= 1
 * otherwise empty array
 * Example:
 * 1 + 1 = 2
 *     1 + 2 = 3
 *         2 + 3 = 5
 *             3 + 5 = 8
 *                 5 + 8 = 13
 *                     8 + 13 = 21
 *                         13 + 21 = 34
 * = [1, 1, 2, 3, 5, 8, 13]
 *
 * @param {number} n - length of the array
 * @returns {Array.<number>} array of fibonacci numbers
*/
function fibonacciNumbersWithLoop(n){
    // If n is 0 return empty array
    if (n <= 0) return [];
    // First 2 values of the fibonacci sequence
    const fibonacciArray = [1, 1];
    // Starting at array index 1, and push value of current index + value of previous index to the array
    for (let i = 1; i < n; i++){
        fibonacciArray.push(fibonacciArray[i] + fibonacciArray[i - 1]);
    }
    return fibonacciArray;
}

/**
 * Creates array of fibonacci numbers using recursive function calls
 * @see {@link fibonacciNumbersWithLoop}
 * @see {@link fib}
 */
function fibonacciNumberRecursively(n){
    const fibonacciArray = [];
    for (let i = 1; i <= n; i++){
        fibonacciArray.push(fib(i));
    }
    return fibonacciArray;
}

/**
 * Function to get fibonacci number for given n:
 * - F(0) = 0
 * - F(1) = 1
 * - F(n) = F(n-1) + F(n-2)
 *   
 * @param {number} n 
 * @returns {number} fibonacci sum of n
*/
function fib(n){
    if (n <= 1) return n;
    else return fib(n - 1) + fib(n - 2);
}

const n1 = 20;
const arrayWithLoop = fibonacciNumbersWithLoop(n1);
console.log(`Fibonacci numbers in array of length ${n1} is [${arrayWithLoop.join(", ")}]`);

const n2 = 10;
const arrayWithRecursive = fibonacciNumberRecursively(n2);
console.log(`Fibonacci numbers in array of length ${n2} is [${arrayWithRecursive.join(", ")}]`);
