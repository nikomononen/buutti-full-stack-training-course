/*
    Shows given number of common words in given book {url from https://www.gutenberg.org/} from the command line
*/
import axios from "axios";
const emptySpacesSplitRegexp = /\s+/;
// eslint-disable-next-line no-unused-vars
const leadingSpecialCharactersTrimRegexp =    /^[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~“”―]+.+$/g;
// eslint-disable-next-line no-unused-vars
const trailingSpecialCharactersTrimRegexp = /^.+[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~“”―]+$/g;
// eslint-disable-next-line no-unused-vars
const allowedCharactersRegexp = /[-a-zA-ZöÖäÄåÅ]+/;
// https://www.gutenberg.org/cache/epub/67403/pg67403.txt
const gutenbergTextBookUrlPattern = /^https:\/\/www\.gutenberg\.org\/cache\/epub\/[0-9]+\/pg[0-9]+\.txt$/;
const ordinals = ["st", "nd", "rd","th"];

/**
 * Checks if the url is valid @see {@link https://www.gutenberg.org/}} url for text file of the book
 * @param  {string} url
 * @returns wether the URL is valid or not
 */
function isValidGutenbergURL(url){
    const isGutenbergURL = gutenbergTextBookUrlPattern.test(url);
    // Is Gutenberg URL so test if it can be made to URL
    if (isGutenbergURL){
        try {
            url = new URL(url);
            return true;
        } catch (_) {
            return false;  
        }
    }
}

/**
 * Counts the given number of most common words in the given data
 * 
 * @param  {string} data - Text data to process
 * @param  {number} numberOfCommonWords - Number of most common words to return
 */
function countMostCommonWords(data, numberOfCommonWords) {
    const rows = data.split("\n");
    const wordsObject = {};
    rows.forEach(row => {
        const words = row.split(emptySpacesSplitRegexp); // Any length of spaces greater than 1
        words.forEach(word => {
            if (word !== ""){
                const wordLowerCase = word.toLocaleLowerCase().trim()
                    .replaceAll(leadingSpecialCharactersTrimRegexp, "") // Remove leading special characters
                    .replaceAll(trailingSpecialCharactersTrimRegexp, ""); // Remove trailing special characters
                //Alternative way is to specify allowed characters
                //const wordLowerCase = word.toLocaleLowerCase().trim().match(allowedCharactersRegexp);
                if (wordLowerCase === undefined || wordLowerCase === null || wordLowerCase.length < 1) return; 
                if(wordLowerCase in wordsObject){
                    wordsObject[wordLowerCase] += 1;
                } else {
                    wordsObject[wordLowerCase] = 1;
                }
            }
        });
    });
    // Create array with [word, count] pairs
    const sortable = [];
    for (const word in wordsObject) {
        sortable.push([word, wordsObject[word]]);
    }
    // Sort descending by the count
    sortable.sort(function(a, b) {
        return b[1] - a[1];
    });
    return sortable.slice(0, numberOfCommonWords);
}

/**
 * Returns ordinal for the given number+
 * 
 * @param  {number} n
 * @returns {string} Ordinal for the given number
 */
function ordinal(n){
    const remainderHundred = n % 100;
    const remainderTen = n % 10;
    if (remainderTen >= 1 && remainderTen <= 3 && ![11, 13, 14].includes(remainderHundred)){
        return ordinals[remainderTen - 1];
    } 
    return ordinals[3];
}

/**
 * List given number of common words in the given book
 * 
 * @param  {string} url - Gutenber URL of the book
 * @param  {number}[numberOfCommonWords=5] - Number of the most common words to list
 */

function listMostCommonWords(url, numberOfCommonWords = 5){
    axios.get(url, {
        // Request headers
        headers: {
            "Content-Type": "text/plain"
        },
    }).then(response => { // Success
        if (response.status === 200){
            const responseText = response.data;
            // Count the words
            const commonWords = countMostCommonWords(responseText, numberOfCommonWords);
            // Print out the results
            console.log("Common words are:");
            commonWords.forEach(([word, count], index) => {
                const n = index + 1;
                const ord = ordinal(n);
                console.log(`${n}${ord} "${word}" (${count})`);
            });
        } else {
            console.log("HTTP response code is not 200 OK");
        }
    }).catch(function (error) { 
        // Error
        console.log(error);
    });
}

// Main
const bookUrl = process.argv[2];
const numberOfCommonWords = parseInt(process.argv[3]);
if(process.argv.length === 3 && isValidGutenbergURL(bookUrl)){
    listMostCommonWords(bookUrl);
} else if(!isNaN(numberOfCommonWords) && numberOfCommonWords > 0 && isValidGutenbergURL(bookUrl)){
    listMostCommonWords(bookUrl, numberOfCommonWords);
} else {
    console.log("Give Gutenberg URL for the text book and number of common words to show. If number is not given, defaults to 5");
    console.log("Example: https://www.gutenberg.org/cache/epub/67403/pg67403.txt 10");
}