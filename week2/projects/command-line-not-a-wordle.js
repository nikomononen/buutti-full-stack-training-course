/**
    Wordle @see {@link https://en.wikipedia.org/wiki/Wordle}
    Players have six attempts to guess a five-letter word, with feedback given for each guess in the form of colored tiles
    indicating when letters match or occupy the correct position. 

    The mechanics are nearly identical to the 1955 pen-and-paper game Jotto and the US television game show Lingo.
    
    Every day, a five-letter word is chosen which players aim to guess within six tries.[1] 
    After every guess, each letter is marked as either green, yellow or gray: 
    - Green indicates that letter is correct and in the correct position
    - Yellow means it is in the answer but not in the right position
    - Gray indicates it is not in the answer at all.
    
    Multiple instances of the same letter in a guess, such as the "o"s in "robot", will be colored green or yellow only
    if the letter also appears multiple times in the answer; otherwise, excess repeating letters will be colored gray.
    
    The game has a "hard mode" option, which requires players to include letters marked as green and yellow in subsequent guesses.

    $ node command-line-not-a-wordle.js 
    You can guess a word by typing <word> + <enter>. Quit by typing 'quit' + <enter>.
    Correct letter at correct place is surrounded by '{}'. Correct letter but not in the correct spot is surrounded by '?'
    You have (6) guesses. TIP: You can as for a random word by typing 'HINT' + <enter>.
    Lets play!
    >aivot
    -------------------------
     ?a?   i    v    o   ?t?
    -------------------------
    >taivas
    Word is not length of 5.
    -------------------------
     ?a?   i    v    o   ?t?
    -------------------------
    >HINT
    Here is a hint 'mieto', you're welcome.
    -------------------------
     ?a?   i    v    o   ?t?
    -------------------------
    >mieto
    -------------------------
     ?a?   i    v    o   ?t?
      m    i    e   {t}   o
    -------------------------
    >laita
    -------------------------
     ?a?   i    v    o   ?t?
     m    i    e   {t}   o
     {l}  {a}   i   {t}  {a}
    -------------------------
    >lauta
    -------------------------
     ?a?   i    v    o   ?t?
      m    i    e   {t}   o
     {l}  {a}   i   {t}  {a}
     {l}  {a}  {u}  {t}  {a}
    -------------------------
    You guessed the Word!!!
    
*/
import xml2js from "xml2js";
import fs from "fs";
import readline from "readline";

// Defaults
const wordLength = 5;
const maxGuesses = 6;

/** 
 * Class representing a word in NotAWordleWord game. 
 */
class NotAWordleWord {
    #word;
    #words;
    #guessedWords;
    #maxGuesses;
    /**
     * NotAWordleWord constructor
     * @param {string} word - Word to guess
     * @param {Array<string>} words - Allowed words
     */
    constructor(word, words){
        this.#word = word;
        this.#words = words;
        this.#guessedWords = new Array();
        this.#maxGuesses = maxGuesses;
    }
    /*
        Character style enum
    */
    static characterStyleEnum = {
        INCORRECT: 0,
        CORRECT: 1,
        CONTAINS: 2
    };
    /**
     * Number of guesses allowed
     * 
     * @returns {number} Number of guesses allowed
     */
    getMaxGuesses = function(){
        return this.#maxGuesses;
    };
    /**
     * Gives random word from words array
     * 
     * @returns {string} Random allowed word
     */
    getHint = function(){
        const randomNumber = Math.floor(Math.random() * (this.#words.length - 0 +1));
        return this.#words[randomNumber];
    };
    /**
     * Returns the word
     * 
     * @returns {string} Word
     */
    getWord = function(){
        return this.#word;
    };
    /**
     * Returns length of the word
     * 
     * @returns {number} Length of the word
     */
    getWordLength = function() {
        return this.#word.length;
    };
    /**
     * Returns guessed words 

     * @returns {Array.<string>} Array of words
     */
    getGuessedWords = function(){
        return this.#guessedWords.map(wordObject => {
            return wordObject.word;
        });
    };
    /**
     * Returns guessed words and wether they are correct, contains 
     * or not in the word to be guessed
     * @returns {Array.<Object>} Array of word objects
     */
    getGuessedWordsStyled = function(){
        return this.#guessedWords.map(wordObject => {
            return wordObject.style;
        });
    };
    /**
     * Guess a word
     * 
     * @param {string} word
     * @returns {boolean} if word can be guessed or not
     */
    guessWord = function(word){
        const wordLowerCase = word.toLowerCase();
        // Check if long enough
        if(word.length !== this.#word.length) return false;
        // Max guesses used
        if(this.checkGameOver()) return false;
        // Is not allowed word
        if(!this.#words.includes(wordLowerCase)) {
            return false;
        }
        // Already guessed 
        const guessedWords = this.getGuessedWords();
        if(guessedWords.includes(wordLowerCase)) {
            return false;
        }
        // Process word to format [["a", 1], ["b", 0] ... ]
        const wordArray = this.#word.split("");
        const characters = wordLowerCase.split("");
        const characterStyleObject = characters.reduce((array, character, index) => {
            let styleEnum;
            if (character === wordArray[index]) styleEnum = NotAWordleWord.characterStyleEnum.CORRECT;
            else if (wordArray.includes(character)) styleEnum = NotAWordleWord.characterStyleEnum.CONTAINS;
            else styleEnum = NotAWordleWord.characterStyleEnum.INCORRECT;
            array.push({
                character: character, 
                notAWordleStyleEnum: styleEnum
            });
            return array;
        }, new Array());
        // Add object to guessed words
        this.#guessedWords.push({
            word: wordLowerCase,
            style: characterStyleObject
        });
        return true;
    };
    /**
     * Check if all the letters in the word is guessed
     * 
     * @returns {boolean} If all letters in the word is already guessed
     */
    checkResolved = function(){
        const guessedWords = this.getGuessedWords();
        return guessedWords.includes(this.#word);
    };
    /**
     * Check if max numbers of guesses is made
     * 
     * @returns {boolean}
     */
    checkGameOver = function(){
        return this.#guessedWords.length >= this.#maxGuesses;
    };
}

/** Class representing NotAWordleGame */
class NotAWordleGame {
    #words;
    #notAWordleWord;
    /**
     * NotAWordleGame constructor. Picks a random word at initialization
     * 
     * @param {Array<String>} words - Array of allowed words
     */
    constructor(words){
        this.#words = words;
        const randomWord = words[this.#getRandomNumber(0, words.length)];
        this.#notAWordleWord = new NotAWordleWord(randomWord, words);
    }
    /**
     * Return a random number withing given range.
     *  
     * @param {number} min
     * @param {number} max
     * @returns {number} random number within given range
     */
    #getRandomNumber = function(min, max){
        const randomNumber = Math.floor(Math.random() * (max - min +1)) + min;
        return randomNumber;
    };
    /** 
     * Prints out instructions
     */
    getInstructions = function(){
        console.log("You can guess a word by typing <word> + <enter>. Quit by typing 'quit' + <enter>.");
        console.log("Correct letter at correct place is surrounded by '{}'. Correct letter but not in the correct spot is surroundrd by '??'");
        console.log(`You have (${this.#notAWordleWord.getMaxGuesses()}) guesses. TIP: You can as for a random word by typing 'HINT' + <enter>.`);
    };
    /**
     * Game view after any input
     */
    #showGameView = function(){
        console.log("-----".repeat(wordLength));
        const wordsToPrint = this.#notAWordleWord.getGuessedWordsStyled();
        if (wordsToPrint.length <= 0 ) return;
        wordsToPrint.forEach(word => {
            const styledCharacterArray = word.map((styleObject) => {
                switch(styleObject.notAWordleStyleEnum){
                    case NotAWordleWord.characterStyleEnum.CORRECT: {
                        return ` {${styleObject.character}} `;
                    }
                    case NotAWordleWord.characterStyleEnum.CONTAINS: {
                        return ` ?${styleObject.character}? `;
                    }
                    default: {
                        // NotAWordle.characterStyleEnum.INCORRECT:
                        return `  ${styleObject.character}  `;
                    }
                }
            });
            console.log(styledCharacterArray.join(""));
        });
        console.log("-----".repeat(wordLength));
    };
    /**
     * Starts the game
     */
    start = function(){
        this.getInstructions();
        console.log("Lets play!");   
        // Node line reader settings
        const consoleLineReader = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
        // Read user input
        consoleLineReader.on("line", (line) => {
            const word = line.trim();
            switch(word){
                case "quit": {
                    console.log("Thanks for playing!");
                    consoleLineReader.close();
                    break;
                }
                case "HINT": {
                    console.log(`Here is a hint '${this.#notAWordleWord.getHint()}', you're welcome.`);
                    this.#showGameView();
                    break;
                }
                default: {
                    if(this.#notAWordleWord.guessWord(word)) {
                        this.#showGameView();
                        // Check if word has been resolved
                        if (this.#notAWordleWord.checkResolved()){
                            console.log("You guessed the Word!!!");
                            consoleLineReader.close();
                            break;
                        }  // Check if word has been resolved
                        else if (this.#notAWordleWord.checkGameOver()){
                            // If not resolved after guess and max wrong answer is reached
                            console.log(`You ran out of guesses. The word was '${this.#notAWordleWord.getWord()}'. Better luck next time!`);
                            consoleLineReader.close();
                            break;
                        }
                    } else {
                        // Word either not long enough, not in allowed words or already guessed
                        if(word.length !== this.#notAWordleWord.getWordLength()) console.log(`Word is not length of ${this.#notAWordleWord.getWordLength()}.`);
                        else if(this.#notAWordleWord.getGuessedWords().includes(word)) console.log("Word is already guessed.");
                        else console.log("Word is not in the allowed words.");
                        this.#showGameView();
                        break;
                    }
                }
            }
        });
    };
}

/* 
    Parse Kotus word list and pick words with specified length

    Format:
    <kotus-sanalista>
        <st><s>aakkonen</s><t><tn>38</tn></t></st>
        <st><s>aakkosellisuus</s><t><tn>40</tn></t></st>
        <st><s>aakkosittain</s><t><tn>99</tn></t></st>
        <st><s>aakkosjärjestys</s></st>
    </kotus-sanalista>
*/
const parser = new xml2js.Parser({ attrkey: "ATTR" });
const xml_string = fs.readFileSync("./kotus-sanalista_v1.xml", "utf8");
parser.parseString(xml_string, function(error, xmlData) {
    if(error) console.log(error);
    const wordsInKotusXML = xmlData["kotus-sanalista"]["st"];
    // Find all words of 5 length 
    const wordsWithSpecifiedLength = wordsInKotusXML.reduce((words, wordObject) => {
        const word = wordObject["s"].shift(); // Get the word from array ["<word>"]
        if(word.length === wordLength){
            words.push(word);
        }
        return words;
    }, new Array());
    // Ready to rumble!!!
    const notAWordleGame = new NotAWordleGame(wordsWithSpecifiedLength);
    notAWordleGame.start();
});