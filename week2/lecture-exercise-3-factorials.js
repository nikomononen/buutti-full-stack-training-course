/**
 * Write a program that contains a function, which calculates
 * the factorial n of a given number n
 * - n! = n*(n-1)*(n-2)*...*1
 * - n=4, n! = 4*3*2*1
 * Note: n! = factorial n
 *
 * @param {number} n - number to factorize
 * @returns {number} factorial
 */
function factorize(n) {
    // If the number is less than 0, reject it.
    if (n < 0) return -1;
    // If the number is 0, factorial is 1.
    else if (n == 0) return 1;
    // Recursive call
    return n * factorize(n - 1);
}

const number = 4;
const factorial = factorize(number);
console.log(`Factorial of ${number} is ${factorial}`);