/**
 * Return a random number withing given range.
 *   
 * @param {number} min
 * @param {number} max
 * @returns {number} random number within given range
 */
function getRandomNumber(min, max){
    const randomNumber = Math.floor(Math.random() * (max - min +1)) + min;
    return randomNumber;
}

const min = 1;
const max = 10;
for (let i = 0; i < 20; i++) {
    const randomNumber = getRandomNumber(min, max);
    console.log(`Random number between ${min} and ${max} is ${randomNumber}`);
}