/**
 * Creates array of fibonacci numbers using recursive function calls
 * @param {n} number to find fibonacci sequence
 * @see {@link fib}
 */
export function fibonacciNumberRecursively(n){
    const fibonacciArray = [0];
    for (let i = 1; i <= n; i++){
        fibonacciArray.push(fib(i));
    }
    return fibonacciArray;
}

/**
 * Function to get fibonacci number for given n:
 * - F(-n) = TypeError("Only positive numbers are allowed")
 * - F(0) = 0
 * - F(1) = 1
 * - F(n) = F(n-1) + F(n-2)
 *   
 * @param {number} n 
 * @returns {number} fibonacci sum of n
*/
export function fib(n){
    if (n < 0) throw new TypeError("Only positive numbers are allowed");
    if (n <= 1) return n;
    return fib(n - 1) + fib(n - 2);
}
