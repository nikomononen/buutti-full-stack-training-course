module.exports = {
    'env': {
        'browser': true,
        'es2021': true,
        'node': true,
    },
    'parser': '@typescript-eslint/parser',
    'plugins': ['@typescript-eslint'],
    'parserOptions': {
        'ecmaVersion': 'latest',
        'sourceType': 'module',
    },
    'extends': [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    'rules': {
        'indent': [
            'error',
            4,
            {'SwitchCase': 1},
        ],
        'linebreak-style': [
            'error',
            'windows',
        ],
        'quotes': [
            'error',
            'single',
        ],
        'semi': [
            'error',
            'always',
        ],
        'no-empty-function': 'off',
        '@typescript-eslint/no-empty-function': 'off',
        '@angular-eslint/no-empty-lifecycle-method': 'off',
        'no-var': 'error',
        'prefer-const': 'error',
        'no-lonely-if': 'error',
        'no-useless-return': 'error',
        'no-constant-condition': 'off',
        'no-redeclare': 'error',
    },
};
