/**
 * Returns sum of a and b
 * @param {int} a 
 * @param {int} b 
 * @returns sum of a and b
 */
export function sum(a, b) {
    return a + b;
}

/**
 * Subtract b from a
 * @param {int} a 
 * @param {int} b 
 * @returns b subtracted from a
 */
export function subtract(a, b) {
    return a - b;
}