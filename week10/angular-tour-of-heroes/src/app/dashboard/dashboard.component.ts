import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  // List of heroes
  heroes: Hero[] = [];
  /*
    // hero
    selectedHero: Hero = {
      id: 1,
      name: 'Windstorm'
    }
  */
  selectedHero?: Hero;

  constructor(private heroService: HeroService) { }

  // Call getHeroes() inside the ngOnInit lifecycle hook and let Angular call ngOnInit() 
  // at an appropriate time after constructing a HeroesComponent instance.
  ngOnInit(): void {
    this.getHeroes();
  }

 /**
  * Get heroes from hero service asynchronously
  */
  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes.slice(1, 5));
  }

}