import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }

  // Messages
  messages: string[] = [];

  /**
   * Add message
   * @param message 
   */
  add(message: string) {
    this.messages.push(message);
  }

  /**
   * Clear messages
   */
  clear() {
    this.messages = [];
  }
}
