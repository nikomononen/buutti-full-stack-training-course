import { Component } from '@angular/core';
import { HeroService } from '../hero.service';
//import { MessageService } from '../message.service';
import { Hero } from '../hero';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent {
  // List of heroes
  heroes: Hero[] = [];
  /*
    // hero
    selectedHero: Hero = {
      id: 1,
      name: 'Windstorm'
    }
  */
  //selectedHero?: Hero;

  constructor(
    private heroService: HeroService, 
    //private messageService: MessageService,
  ) { }

  // Call getHeroes() inside the ngOnInit lifecycle hook and let Angular call ngOnInit() 
  // at an appropriate time after constructing a HeroesComponent instance.
  ngOnInit(): void {
    this.getHeroes();
  }

  /**
   * Add new hero to database
   * @param name Hero name
   * @returns 
   */
  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.addHero({ name } as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }
  
  /**
   * Delete hero from database
   * @param hero hero id
   */
  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero.id).subscribe();
  }
  /*
  // Get heroes from hero service synchronously
  getHeroes(): void {
    this.heroes = this.heroService.getHeroes();
  }
  */

 /**
  * Get heroes from hero service asynchronously
  */
  getHeroes(): void {
    this.heroService.getHeroes()
        .subscribe(heroes => this.heroes = heroes);
  }

 /**
  * Set selected hero when clicked
  
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
  }
  */
}
