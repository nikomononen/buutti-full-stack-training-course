import { render, screen, within, fireEvent, TestingLibraryElementError } from "@testing-library/react";
import App from "./App.js";

describe("<App />", () => {
  test("renders textbox with empty value", () => {
    render(<App />);

    const textBox = screen.getByRole("textbox");

    expect(textBox).toBeInTheDocument();
    expect(textBox).toHaveTextContent("")
    expect(textBox.value).toBe("")
  });

  test("renders submit button", () => {
    render(<App />);

    const submitButton = screen.getByRole("button");

    expect(submitButton).toBeInTheDocument();
  });

  test("renders empty list when initialized", () => {
    render(<App />);

    // Get list
    const list = screen.getByRole("list", {
      name: "items",
    });

    const { getAllByRole } = within(list)
    const t = () => {
      const listItems = getAllByRole("listitem");
    }

    expect(t).toThrow(TestingLibraryElementError);
  });

  /**
   * The test should 
   * 1) locate the input field, 
   * 2) write a value to the field, 
   * 3) submit the form and 
   * 4) check that after submitting the form, the name exists on the page 
   *    and that the input field is empty.
   */
  test('writes text to the input and submits form. Text appears on the list and input is ""', () => {
    render(<App />);

    const textBox = screen.getByRole("textbox");
    const submitButton = screen.getByRole("button");

    // Write text to input
    fireEvent.change(textBox, {target: {value: "test"}});

    // Textbox is not empty after user input
    expect(textBox.value).toBe("test");

    // Submit form
    fireEvent.submit(submitButton);

    // Textbox is empty after submit
    expect(textBox).toHaveTextContent("");

    // Get list
    const list = screen.getByRole("list", {
      name: "items",
    });
    
    const { getAllByRole } = within(list)
    const listItems = getAllByRole("listitem");

    expect(listItems.length).toBe(1);
    expect(listItems[0].innerHTML).toBe("test");
  });

});