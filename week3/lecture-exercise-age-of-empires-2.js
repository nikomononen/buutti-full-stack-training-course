"use strict";
const apiPath = "http://localhost:3000/";
const resources = [
    {
        name: "Civilizations",
        path: "civilizations"
    },
    {
        name: "Structures",
        path: "structures"
    },{
        name: "Technologies",
        path: "technologies"
    },
    {
        name: "Units",
        path: "units"
    },
];

function createSelect(options){
    const select = document.createElement("select");
    options.forEach((restOption) => {
        const option =  document.createElement("option");
        option.value = restOption.path;
        option.text = restOption.name;
        select.appendChild(option);
    });
    return select;
}

function createList(items){
    const ul = document.createElement("ul");
    if (items.length === 1){
        return document.createTextNode(items[0]);
    }
    items.forEach(item => {
        const li = document.createElement("li");
        li.appendChild(document.createTextNode(item));
        ul.appendChild(li);
    });
    return ul;
}
function createElement(data){
    const div = document.createElement("div");
    const table = document.createElement("table");
    table.classList.add("data-table");
    const trTH = document.createElement("tr");
    const fieldTH= document.createElement("th");
    const valueTH= document.createElement("th");
    fieldTH.appendChild(document.createTextNode("Key"));
    valueTH.appendChild(document.createTextNode("Value"));
    trTH.appendChild(fieldTH);
    trTH.appendChild(valueTH);
    table.appendChild(trTH);
    Object.entries(data).forEach(([key, value]) => {
        const tr = document.createElement("tr");
        const fieldTD= document.createElement("td");
        const valueTD= document.createElement("td");
        fieldTD.appendChild(document.createTextNode(key.toLocaleUpperCase()));
        if (Array.isArray(value)){
            valueTD.appendChild(createList(value));
        }
        else if ((typeof value === "object" || typeof value === "function") && (value !== null)){
            // Todo printout Object nicely
            valueTD.appendChild(document.createTextNode(JSON.stringify(value)));
        } else {
            valueTD.appendChild(document.createTextNode(value));
        }
        tr.appendChild(fieldTD);
        tr.appendChild(valueTD);
        table.appendChild(tr);
    });
    div.appendChild(table);
    return div;
}

function doRestCall(event){
    const select = event.target;
    selectDataDiv.innerHTML = "";
    const url = apiPath + select.value;
    const options = {
        method: "GET",
        headers: {
            "Access-Control-Allow-Origin": "*",
            Accept: "application/json",
        },
    };
    fetch(url, options)
        .then(response => response.json())
        .then(data =>  {
            const dataList = data[select.value];
            dataList.forEach(data =>{
                selectDataDiv.appendChild(createElement(data));
            });
        })
        .catch(error => console.error(error));
}

const selectDiv = document.getElementById("data-select-div");
const selectDataDiv = document.getElementById("select-data-div");
const select = createSelect(resources);
select.addEventListener("change", doRestCall);
select.dispatchEvent(new CustomEvent("change"));
selectDiv.appendChild(select);