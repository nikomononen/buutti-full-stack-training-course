"use strict";

const colors = {
    red: "red",
    green: "green",
    blue: "blue",
    lightGray: "#D3D3D3",
    white: "white"
};
const colorsKeyList = Object.keys(colors);

const changePageBackgroundColor = function(){
    const randomColorKey = colorsKeyList[Math.floor(Math.random() * colorsKeyList.length)];
    document.body.style.backgroundColor = colors[randomColorKey];
};

document.getElementById("changeBackgroundColorButton").addEventListener("click", changePageBackgroundColor);