import jsonServer from "json-server";

const server = jsonServer.create();
const router = jsonServer.router("week3/lecture-day-2-json-server-db.json");
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(router);

server.listen(3000, () => {
    console.log("JSON server is running");
});
