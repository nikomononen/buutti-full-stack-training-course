import axios from "axios";
import express from "express";
import cors from "cors";
const port = 3000;

const app = express();
app.use(cors());

app.get("*", (req, res) => {
    console.log(req.path);
    axios.get("https://age-of-empires-2-api.herokuapp.com/api/v1/" + req.path)
        .then(data => {
            res.send(data.data);
        })
        .catch(error => console.error(error));
});

app.listen(port);
console.log("listening 3000");