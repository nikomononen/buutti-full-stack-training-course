/* eslint-disable no-undef */
"use strict";
const displayElement = document.getElementById("jokesDisplayDiv");

function clearChildNodes(element){
    while (element.firstChild) {
        element.firstChild.remove();
    }
}
function createJokeElement(elementType, joke){
    const span = document.createElement("span");
    span.innerText = `${joke.id}`;
    span.classList.add("joke-id");
    const text = document.createTextNode(joke.joke);
    const element = document.createElement(elementType);
    element.appendChild(span);
    element.appendChild(text);
    return element;
}
const displayRandomJoke = function(){
    clearChildNodes(displayElement);
    const randomJoke = jokes[Math.floor(Math.random() * jokes.length)];
    const jokeElement = createJokeElement("p", randomJoke);
    displayElement.appendChild(jokeElement);
};
const displayRandomNerdJoke = function(){
    clearChildNodes(displayElement);
    const nerdJokes = jokes.filter(joke => joke.categories.includes("nerdy"));
    const randomNerdJoke = nerdJokes[Math.floor(Math.random() * nerdJokes.length)];
    const jokeElement = createJokeElement("p", randomNerdJoke);
    displayElement.appendChild(jokeElement);
};
const displayAllJokes = function(){
    clearChildNodes(displayElement);
    for(const joke of jokes){
        const jokeElement = createJokeElement("p", joke);
        displayElement.appendChild(jokeElement);
    }
};
// Add listeners
document.getElementById("randomJokeButton").addEventListener("click", displayRandomJoke);
document.getElementById("randomNerdJokeButton").addEventListener("click", displayRandomNerdJoke);
document.getElementById("allJokesButton").addEventListener("click", displayAllJokes);
